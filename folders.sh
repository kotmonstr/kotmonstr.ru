#!/usr/bin/env bash
echo "Создание директорий"
set -x
sudo mkdir ./frontend/web/upload
sudo mkdir ./frontend/web/upload/multy-big
sudo mkdir ./frontend/web/upload/multy-thumbs
sudo mkdir ./console/runtime/cache
sudo chmod -v 0777 -R ./frontend/web/upload
sudo chmod 777 -R frontend/
sudo chmod 777 -R backend/
sudo chmod 777 -R console/
sudo chmod 777 -R common/

echo "Все "