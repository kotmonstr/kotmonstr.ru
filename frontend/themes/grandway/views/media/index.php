<?php

use yii\helpers\Html;
use app\components\PhotoHelper;
use app\components\DirHelper;

$uploudPath = '/web/upload/';
$this->title = 'Media';
$this->params['breadcrumbs'][] = $this->title;
$dirObject = new DirHelper();



    //This is  a more readable way of viewing the returned float

    // $Bytes contains the total number of bytes on "/"
    $Bytes = disk_total_space("/");

    function dataSize($Bytes)
    {
        $Type=array("", "kilo", "mega", "giga", "tera");
        $counter=0;
        while($Bytes>=1024)
        {
            $Bytes/=1024;
            $counter++;
        }
        return("".$Bytes." ".$Type[$counter]."bytes");
    }




?>

<div class="box ">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) .'    Осталось места : ' . dataSize($Bytes); ?></h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>






    <div class="box-body" style="display: block;">

        <div class="table-responsive">
            <table class="table table-bordered table-responsive table-hover">
                <? if ($place == 'IN') {
                    echo '<tr>';
                    echo "<td>";
                    echo '<p><a href="/media/index?url=">.</a></p>';
                    echo '<p><a href="/media/index?url=' . $dirObject->Up($url) . '">..</a></p>';
                    echo '<img src="/images/dir.jpg" alt="" width="25px">';
                    echo '<strong>' . $url . '</strong>'.' ';
                    echo !empty($fileList) ? '<a href="/media/index?url=' . $url . '&deleteall=true" class="btn btn-danger pull-right"> Delete all Photos!</a>' : '';
                    echo "</td>";
                    echo "</tr>";
                }
                ?>

                <?php $dirObject->getDirs($dirList);// Dir ?>


                <?php
                // filers
                foreach ($fileList as $file):
                    $pos = strrpos($file['shot'], "/");
                    $shotFileName = str_replace("/", "", substr($file['shot'], $pos));
                    $isItDeletable = PhotoHelper::isPhotoDeletable($shotFileName);
                    $fullFileName = '/' . str_replace(Yii::getAlias('@frontend') . $uploudPath, "", $file['full']);
                    echo '<tr>';
                    echo '<td>';
                    echo '<img src="/upload/' . $file['shot'] . '" alt="" width="35px" height="35px">';
                    echo ' ' . $shotFileName . ' ';
                    echo !$isItDeletable ? '<a href="/media/index?url=' . $url . '&delete=' . $fullFileName . '" class="btn btn-warning"><span class="glyphicon glyphicon-remove" ></span></a>' : '<span style="color: green;font-weight:bold">' . $isItDeletable . '</span>';
                    echo '<a href="/media/index?url=' . $url . '&delete=' . $fullFileName . '" class="btn btn-danger"><span class="glyphicon glyphicon-remove" >Warning!!!</span></a>';
                    echo '</td>';
                    echo "</tr>";
                endforeach;
                ?>
            </table>
        </div>
    </div>
</div>