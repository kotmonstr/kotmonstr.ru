<?php

use yii\helpers\Html;

?>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="welcome">
                <div class="page-name pull-left"><h3>Чат</h3>
                    <br>
                    <span>Статус соединения: </span><span id="chat-status"></span>
                </div>
                <div class="page-link pull-right"><a href="/">Главная</a> / Чат</div>
            </div>
        </div>
    </div>
</div>
<input id="user" class="hidden" value="<?= isset(Yii::$app->user->identity->username) ? Yii::$app->user->identity->username : 'anonyme' ?>">

<div class="container marg25">
    <div class="row" style="min-height: 300px">
        <div id="list" class="col-md-12"></div>

        <?= Html::tag('div', Html::textarea('message', '', ['id' => 'message', 'class' => 'form-control', 'rows' => 3]), ['class' => 'col-md-10 textarea']); ?>
        <?= Html::tag('div', Html::input('button', 'submit', 'Отправить', ['id' => 'submit', 'class' => 'btn btn-default'], ['class' => 'col-md-2'])); ?>

    </div>
</div>

<?php
$script = <<< JS

window.onload = function(){
    $('#message').focus();
    var socket = new WebSocket('ws://http://localhost:8181');
    socket.onopen = function(event){
        $('#chat-status').html('Соединился');
    }
 
    socket.onclose = function(event){
        
        if(event.wasClean){
             $('#chat-status').html('Отсоеденился корректно');
        }
        else{
             $('#chat-status').html('Отсоеденился по ошибке ' + event.code + 'причина ' + event.reason);
        }
       
    }

    socket.onmessage = function(event){
        var data = JSON.parse(event.data);
        console.log(data.user );
        
        $('#list').append('<div class="item well"><strong><p>' + data.user + '</strong>: ' + data.msg + '</p><button class="btn btn-default" onclick="alert(1)">Принять заказ</button>'+ '<span class="time">'+getNowTime()+'</span></div>');
    }
    
    socket.onerror= function(event){
        $('#status').html('Ошибка');
    }
    
    $('#submit').click(function(){
        var message = {
            user: $('#user').val(),
            msg: $('#message').val()
        }
              //console.log(JSON.stringify(message));
               socket.send(JSON.stringify(message));
         $('#message').val('');
       $('#message').focus();
    });
    

}
function getNowTime(){
    
var currentdate = new Date(); 
var datetime =  + currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + "  "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();
return datetime;
}


JS;
$this->registerJs($script);
?>
<style>
    .time {
        color: #a9a4a4;
    }

    .item {
        margin: 5px;
        padding: 5px;
    }

    .textarea {
        padding: 20px;
    }

    #submit {
        margin-top: 60px;
        margin-left: 10px;
    }
</style>




