<?php
use app\components\RecentGoodsWidget;
use app\components\ReqvizitWidget;
use app\components\NewsWidget;

?>




<?= NewsWidget::widget() ?>


<div class="footer">

        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <div class="footer-block"><img src="/GrandWay/assets/images/logo_foot.png" alt="" class="foot_marg">
                    </div>
                    <div class="foot-text">Сайт с позновательным контентом.</div>
                    <div class="foot-text">

                    </div>
                </div>
                <div class="col-lg-3 col-md-3">
                    <div class="footer-block">Контакты</div>
                    <?= ReqvizitWidget::widget() ?>
                </div>
                <div class="col-lg-3 col-md-3">
                    <?= RecentGoodsWidget::widget(['template' => 'footer', 'limit' => 9]) ?>
                </div>
                <div class="col-lg-3 col-md-3">
                    <div class="footer-block">Flickr Widget</div>
                    <div class="widget_flickr">
                        <div class="flickr_container">
                            <script type="text/javascript"
                                    src="http://www.flickr.com/badge_code_v2.gne?count=6&amp;display=random&amp;size=s&amp;layout=x&amp;source=user&amp;user=146823312@N06"></script>
                        </div>
                    </div>
                </div>
            </div>
        </div>

</div>