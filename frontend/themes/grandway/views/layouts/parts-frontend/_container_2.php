<div class="container marg75">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="title"><span>Recent Projects</span></h3>
            <div class="pagination">
                <a href="" id="portfolio_left" class="prev"></a>
                <a href="" id="portfolio_right" class="next"></a>
            </div>
        </div>
        <div class="col-lg-12">
            <div id="portfolio" class="marg25">
                <div class="showbiz" data-left="#portfolio_left" data-right="#portfolio_right">
                    <div class="overflowholder">
                        <ul>
                            <li class="portfolio-block">
                                <div class="mediaholder">
                                    <div class="mediaholder_innerwrap">
                                        <a href="#"><img alt="" src="/GrandWay/assets/images/11.jpg"></a>
                                        <div class="hovercover" data-maxopacity="0.85">
                                            <a href="#"><div class="linkicon notalone"><i class="fa fa-link"></i></div></a>
                                            <a class="fancybox" rel="group2" href="/GrandWay/assets/images/11.jpg"><div class="lupeicon notalone"><i class="fa fa-search"></i></div></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="detailholder">
                                    <div class="portfolio-name">Michela Chiucini</div>
                                    <div class="portfolio-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit illo voluptas aperiam impedit quidem</div>
                                </div>
                            </li>
                            <li class="portfolio-block">
                                <div class="mediaholder">
                                    <div class="mediaholder_innerwrap">
                                        <a href="#"><img alt="" src="/GrandWay/assets/images/88.jpg"></a>
                                        <div class="hovercover" data-maxopacity="0.85">
                                            <a href="#"><div class="linkicon notalone"><i class="fa fa-link"></i></div></a>
                                            <a class="fancybox" rel="group2" href="/GrandWay/assets/images/88.jpg"><div class="lupeicon notalone"><i class="fa fa-search"></i></div></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="detailholder">
                                    <div class="portfolio-name">Attack Pattern</div>
                                    <div class="portfolio-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit illo voluptas aperiam impedit quidem</div>
                                </div>
                            </li>
                            <li class="portfolio-block">
                                <div class="mediaholder">
                                    <div class="mediaholder_innerwrap">
                                        <a href="#"><img alt="" src="/GrandWay/assets/images/77.jpg"></a>
                                        <div class="hovercover" data-maxopacity="0.85">
                                            <a href="#"><div class="linkicon notalone"><i class="fa fa-link"></i></div></a>
                                            <a class="fancybox" rel="group2" href="/GrandWay/assets/images/77.jpg"><div class="lupeicon notalone"><i class="fa fa-search"></i></div></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="detailholder">
                                    <div class="portfolio-name">Cinnamon Toast</div>
                                    <div class="portfolio-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit illo voluptas aperiam impedit quidem</div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>