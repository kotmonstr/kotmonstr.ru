<?php if( Yii::$app->params['SHOP_STATUS'] == 1): ?>

<div class="container marg75">
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="col-lg-9 col-md-9">
                    <h3>ONLINE магазин</h3>
                    <p>Здесь можно просмотреть ассортимент товаров или сделать покупку.</p>
                </div>
                <div class="col-lg-3 col-md-3">
                    <div class="purch-block">
                        <a class="btn btn-purch btn-lg" target="_blank" href="/goods">Сделать покупки!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php endif; ?>