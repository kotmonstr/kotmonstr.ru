<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\GrandwayAsset;
use frontend\assets\GrandwayAssetNoSnow;
use common\widgets\Alert;
use app\components\SeoHelper;

if ($this->beginCache('view')) {
    Yii::$app->params['SNOW_IN_LAYOUT'] == 1 ? GrandwayAsset::register($this) : GrandwayAssetNoSnow::register($this);
    $this->endCache();
}


?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/GrandWay/assets/images/favicon.ico" rel="shortcut icon"/>
    <?= Html::csrfMetaTags() ?>
    <?= SeoHelper::getMeta(); ?>
    <?php $this->head() ?>
</head>
<body>

<?php if(Yii::$app->params['SNOW_IN_LAYOUT'] == 1): ?>
    <div id="particles-js"></div>
<?php endif; ?>
<div id="wrapper">

    <?php $this->beginBody() ?>

    <?= $this->render('parts-frontend/_header'); ?>
    <?= $this->render('parts-frontend/_page_head'); ?>
    <?//= $this->render('parts-frontend/_tp-banner-container'); ?>
    <?//= $this->render('parts-frontend/_container_1'); ?>
    <?//= $this->render('parts-frontend/_container_2'); ?>
    <?//= $this->render('parts-frontend/_container_3'); ?>
    <?//= $this->render('parts-frontend/_posts'); ?>
    <?///= $this->render('parts-frontend/_container_4'); ?>
    <?//= $this->render('parts-frontend/_container_5'); ?>
    <?//= $this->render('parts-frontend/_twitter'); ?>



    <?= $content ?>


    <?= $this->render('parts-frontend/_footer'); ?>
    <?= $this->render('parts-frontend/_footer-bottom'); ?>

    <button onclick="topFunction()" id="myBtn" title="На верх">Top</button>


</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

