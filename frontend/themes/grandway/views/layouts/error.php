<?php


use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\GoodsAsset;
use common\widgets\Alert;
use app\components\SliderWidget;
use frontend\assets\GrandwayAssetNoSnow;
use frontend\assets\GrandwayAsset;

if ($this->beginCache('view')) {
    Yii::$app->params['SNOW_IN_LAYOUT'] == 1? GrandwayAsset::register($this) : GrandwayAssetNoSnow::register($this);
    $this->endCache();
}

?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>

    <?= $this->render('parts-frontend/_mainmenu-wrapper'); ?>
    <?= $content ?>

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>