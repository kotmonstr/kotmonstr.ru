<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\GrandwayAsset;
use frontend\assets\GrandwayAssetNoSnow;
use common\widgets\Alert;
use app\components\SliderWidget;
use app\components\RecentArticlesWidget;
use app\components\RecentGoodsWidget;
use app\components\SeoHelper;

if ($this->beginCache('view')) {
    Yii::$app->params['SNOW_IN_LAYOUT'] == 1 ? GrandwayAsset::register($this) : GrandwayAssetNoSnow::register($this);
    $this->endCache();
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="GrandWay/assets/images/favicon.ico" rel="shortcut icon"/>
    <?= Html::csrfMetaTags() ?>
    <?= SeoHelper::getMeta($this->context->meta); ?>
    <meta name="yandex-verification" content="2ae4916cf2cf9ac9" />


    <?php $this->head() ?>

</head>
<body >

<?php if ($this->beginCache('view2')) { ?>

    <?php if(Yii::$app->params['SNOW_IN_LAYOUT'] == 1): ?>
        <div id="particles-js"></div>
    <?php endif; ?>

<?php $this->endCache();
}

?>

<div id="wrapper">

<?php $this->beginBody() ?>

<?= $this->render('parts-frontend/_header'); ?>
<?= $this->render('parts-frontend/_page_head'); ?>

<?= SliderWidget::widget(['theme' => 'grandway']); ?>

<?//= $this->render('parts-frontend/_tp-banner-container'); ?>
<?= $this->render('parts-frontend/_container_1'); ?>

<?= RecentArticlesWidget::widget() ?>

<?= $this->render('parts-frontend/_container_3'); ?>
<?//= $this->render('parts-frontend/_posts'); ?>
<?//= $this->render('parts-frontend/_container_4'); ?>

<?= RecentGoodsWidget::widget(['template'=>'sidebar']) ?>
<?//= $this->render('parts-frontend/_container_5'); ?>

<?//= $this->render('parts-frontend/_twitter'); ?>



        <?//= $content ?>

<?php if ($this->beginCache('view3')) { ?>

<?= $this->render('parts-frontend/_footer'); ?>

<?= $this->render('parts-frontend/_footer-bottom'); ?>
    <?php $this->endCache();
}

?>
    <button onclick="topFunction()" id="myBtn" title="На верх">Top</button>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<script type="text/javascript">
    var revapi;
    jQuery(document).ready(function () {
        revapi = jQuery('.tp-banner').revolution({
            delay: 3000,
            startwidth: 1170,
            startheight: 500,
            //hideThumbs: 10,
            forceFullWidth: "off",
            navigation : {
                keyboardNavigation:"on",
                keyboard_direction:"horizontal",		//	horizontal - left/right arrows,  vertical - top/bottom arrows
                mouseScrollNavigation:"off",
                onHoverStop:"off",						// Stop Banner Timet at Hover on Slide on/off

                touch:{
                    touchenabled:"on",						// Enable Swipe Function : on/off
                    swipe_treshold : 75,					// The number of pixels that the user must move their finger by before it is considered a swipe.
                    swipe_min_touches : 1,					// Min Finger (touch) used for swipe
                    drag_block_vertical:false,				// Prevent Vertical Scroll during Swipe
                    swipe_direction:"horizontal"
                },
                arrows: {
                    style:"",
                    enable:true,
                    hide_onmobile:false,
                    hide_onleave:true,
                    hide_delay:200,
                    hide_delay_mobile:1200,
                    hide_under:0,
                    hide_over:9999,
                    tmp:'',
                    left : {
                        h_align:"left",
                        v_align:"center",
                        h_offset:20,
                        v_offset:0,
                    },
                    right : {
                        h_align:"right",
                        v_align:"center",
                        h_offset:20,
                        v_offset:0
                    }
                },
                bullets: {
                    style:"",
                    enable:false,
                    hide_onmobile:false,
                    hide_onleave:true,
                    hide_delay:200,
                    hide_delay_mobile:1200,
                    hide_under:0,
                    hide_over:9999,
                    direction:"horizontal",
                    h_align:"left",
                    v_align:"center",
                    space:0,
                    h_offset:20,
                    v_offset:0,
                    tmp:'<span class="tp-bullet-image"></span><span class="tp-bullet-title"></span>'
                },
                thumbnails: {
                    style:"",
                    enable:true,
                    width:100,
                    height:50,
                    min_width:100,
                    wrapper_padding:2,
                    wrapper_color:"#f5f5f5",
                    wrapper_opacity:1,
                    tmp:'<span class="tp-thumb-image"></span><span class="tp-thumb-title"></span>',
                    visibleAmount:5,
                    hide_onmobile:false,
                    hide_onleave:true,
                    hide_delay:200,
                    hide_delay_mobile:1200,
                    hide_under:0,
                    hide_over:9999,
                    direction:"horizontal",
                    span:false,
                    position:"inner",
                    space:2,
                    h_align:"left",
                    v_align:"center",
                    h_offset:20,
                    v_offset:0
                },
                tabs: {
                    style:"",
                    enable:true,
                    width:100,
                    min_width:100,
                    height:50,
                    wrapper_padding:10,
                    wrapper_color:"#f5f5f5",
                    wrapper_opacity:1,
                    tmp:'<span class="tp-tab-image"></span>',
                    visibleAmount:5,
                    hide_onmobile:false,
                    hide_onleave:true,
                    hide_delay:200,
                    hide_delay_mobile:1200,
                    hide_under:0,
                    hide_over:9999,
                    direction:"horizontal",
                    span:false,
                    space:0,
                    position:"inner",
                    h_align:"left",
                    v_align:"center",
                    h_offset:20,
                    v_offset:0
                }
            },
        });
    }); //ready

</script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-96404263-1', 'auto');
    ga('send', 'pageview');

</script>
