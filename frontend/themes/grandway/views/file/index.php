<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\FilesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Файлы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box ">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
    <div class="box-body" style="display: block;">

        <div class="table-responsive">




            <p>
                <?= Html::a('Создать файл', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?php
            $options = [
                'title' => 'My Super Link',
                'target' => '_blank',
                'alt' => 'Link to Super Website',
            ];
            ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'file_name',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return Html::a( $data->file_name , $data['image_path'].'/'.$data->file_name,['target' => '_blank']);
                        },
                    ],
                    //'file_path',
                    'file_title',
                    'file_type',
                     //'image',
                    [
                        'attribute' => 'image',
                        'format' => 'html',
                        'value' => function ($data) {
                            return Html::img(Yii::getAlias('@web'). $data['image_path'].'/'.$data->image,['width' => '70px']);
                        },
                    ],
                    // 'descr',
                    'created_at',
                    // 'updated_at',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>


        </div>
    </div>
</div>

