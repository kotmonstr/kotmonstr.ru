<?php
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\widgets\LinkPager;
use yii\helpers\Url;
?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="welcome">
                <div class="page-name pull-left"><h3>Видео</h3></div>
                <div class="page-link pull-right"><a href="/">Главная</a> / Видео</div>
            </div>
        </div>
    </div>
</div>
<div class="container marg25">
    <div class="row">
        <div class="col-lg-3">
            <h3 class="title-in"><span>Поиск</span></h3>
            <input type="text" name="s" class="form-control searchform" value="">
            <h3 class="title-in"><span>Темы</span></h3>
            <ul class="categories">

                <? if ($modelVideoAlbum): ?>
                    <li><a class="<?= !isset($currentSlug) ? 'active-item' : null ?>" href="/site/video"> Все </a></li>
                    <? foreach ($modelVideoAlbum as $album): ?>
                        <li><a class="<?= isset($currentSlug) && $currentSlug == $album->slug ? 'active-item' : null ?>" href="<?= Url::to(['/site/video', 'slug' => $album->slug]) ?>"><?= $album->name ?></a></li>
                    <? endforeach ?>
                <? endif; ?>

            </ul>
            <h3 class="title-in"><span>Свежие видео</span></h3>
            <div class="tag_cloud_blog">

                <? if ($modelVideoAlbumLast): ?>
                    <? foreach ($modelVideoAlbumLast as $album): ?>
                        <a class="<?= isset($currentSlug) && $currentSlug == $album->slug ? 'active-item-tag' : null ?>" href="<?= Url::to(['/site/video', 'slug' => $album->slug]) ?>"><?= $album->name ?></a>
                    <? endforeach ?>
                <? endif; ?>

            </div>
            <h3 class="title-in"><span>Архив</span></h3>
            <ul class="categories">
                <? if($arrMonths):?>
                    <? foreach ($arrMonths as $date): ?>
                        <li>
                            <a href="<?= Url::to(['/site/video-date', 'date' => str_replace(" ","-",$date) ]); ?> " class="<?= $curDate == str_replace(" ","-",$date) ? 'active-item' : null ?>"><?= $date ?></a>
                        </li>
                    <? endforeach; ?>
                <? endif ?>
            </ul>
        </div>
        <div class="col-lg-9">
            <div class="portfolioFilter">
                <ul>
                    <? if ($modelVideoAlbum): ?>

                        <li><a href="/site/video" class="<?= !isset($currentSlug) ? 'current' : null ?>"> Все </a></li>

                        <? foreach ($modelVideoAlbum as $album): ?>
                            <li><a href="<?= Url::to(['/site/video', 'slug' => $album->slug]) ?>"
                                   data-hover="<?= $album->name ?>" data-filter="*"
                                   class="<?= $album->slug == $currentSlug ? 'current' : null ?>"><?= $album->name ?></a>
                            </li>
                        <? endforeach ?>
                    <? endif; ?>
                </ul>
            </div>
            <div class="row marg50">
                <div class="showbiz">
                    <div class="portfolio overflowholder">

                        <? if ($model): ?>
                            <? foreach ($model as $youtube): ?>

                                <div class="port-4 photoshop php wordpress">
                                    <div class="portfolio-block-in">
                                        <div class="mediaholder">
                                            <div class="mediaholder_innerwrap">
                                                <iframe class="fancybox" width="100%" height="200px" src="https://www.youtube.com/embed/<?= $youtube->youtube_id ?>" frameborder="0" allowfullscreen></iframe>
                                            </div>
                                        </div>
                                        <div class="detailholder">

                                            <div class="portfolio-text"><?= StringHelper::truncate($youtube->title,40) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <? endforeach; ?>
                        <? else: ?>

                            <div class="col-md-4 col-md-offset-5">
                                <p>Список пуст.</p>
                            </div>

                        <? endif ?>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12 marg50">
            <div class="pagin" style="float:right;">
                <?= LinkPager::widget([
                    'pagination' => $pages,
                    'hideOnSinglePage' => true,
                    'activePageCssClass' => 'current-my',
                    'disabledPageCssClass' => 'current-my-disable',
                    'maxButtonCount' => 5,
                    //'prevPageLabel' => '&laquo;',
                    //'nextPageLabel' => '&raquo;',

                ]);
                ?>
            </div>
        </div>



    </div>
</div>












