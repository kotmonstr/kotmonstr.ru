<?php
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use common\models\GoodsPodCategory;
use frontend\assets\GrandwayAsset;

$this->registerJsFile('/GrandWay/assets/js/dropdown-grandway.js', ['depends' => GrandwayAsset::className()]);

$i = 0;
?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="welcome">
                <div class="page-name pull-left"><h3>Товары</h3></div>
                <div class="page-link pull-right"><a href="/">Главная</a> / Товары</div>
            </div>
        </div>
    </div>
</div>
<div class="container marg25">
    <div class="row">
        <div class="col-lg-3">
            <h3 class="title-in"><span>Поиск</span></h3>
            <input type="text" name="s" class="form-control searchform" value="">
            <h3 class="title-in"><span>Категории</span></h3>
            <ul class="categories">

                <?php if (isset($modelGoodsCategory)): ?>

                    <li><a class="" href="/site/goods">Все</a></li>

                    <?php foreach ($modelGoodsCategory as $category): ?>

                        <?php $i++; ?>
                        <li><a class="mainCat" date-parent="<?= $i; ?>" href="javascript:void(0);"><?= $category->name ?></a></li>

                        <?php // вывести подкатегории
                        $modPodCategory = GoodsPodCategory::getAllByCatId($category->id); ?>
                        <?php if ($modPodCategory): ?>
                            <?php foreach ($modPodCategory as $podCat): ?>
                                <ul class="categories child-<?= $i; ?>">
                                    <li>
                                        <a class="<?= isset($currentCategory) && $currentCategory == $podCat->slug ? 'active-item' : null ?>" href="<?= Url::to(['site/goods', 'pod-category' => $podCat->slug]); ?>"><?= $podCat->name ?></a>
                                    </li>
                                </ul>
                            <?php endforeach; ?>
                        <?php endif; ?>

                    <?php endforeach ?>
                <?php endif; ?>

            </ul>
            <h3 class="title-in"><span>Новинки</span></h3>
            <div class="tag_cloud_blog">

                <?php if (isset($modelGoodLast)): ?>
                    <?php foreach ($modelGoodLast as $good): ?>
                        <a class="<?= isset($currentSlug) && $currentSlug == $good->item ? 'active-item-tag' : null ?>"
                           href="<?= Url::to(['/site/goods-detail', 'slug' => $good->slug]) ?>"><?= $good->item ?></a>
                    <?php endforeach ?>
                <?php endif; ?>

            </div>
            <h3 class="title-in"><span>Архив</span></h3>
            <ul class="categories">
                <?php if ($arrMonths): ?>
                    <?php foreach ($arrMonths as $date): ?>
                        <li>
                            <a href="<?= Url::to(['/site/goods-date', 'date' => str_replace(" ", "-", $date)]); ?>" class="<?= $curDate == str_replace(" ","-",$date) ? 'active-item' : null ?>"><?= $date ?></a>
                        </li>
                    <?php endforeach; ?>
                <?php endif ?>
            </ul>
        </div>
        <div class="col-lg-9">

            <div class="row marg50">
                <div class="showbiz">
                    <div class="portfolio overflowholder">

                        <?php if (!empty($model)): ?>
                            <?php foreach ($model as $galerySlide): ?>

                                <div class="port-4 photoshop php wordpress">
                                    <div class="portfolio-block-in">
                                        <div class="mediaholder">
                                            <div class="mediaholder_innerwrap">
                                                <a href="#"><?= Html::a(Html::img('/upload/goods/' . $galerySlide->image, []), '/upload/goods/' . $galerySlide->image, ['class' => 'fancybox', 'title' => $galerySlide->descr, 'rel' => 'fancybox-thumb']) ?></a>
                                                <div class="hovercover" data-maxopacity="0.85">
<!--                                                    <a href="--><?//= Url::to('/site/good-detail'); ?><!--">-->
<!--                                                        <div class="linkicon notalone"><i class="fa fa-link"></i></div>-->
<!--                                                    </a>-->
                                                    <a class="fancybox" rel="group2"
                                                       href="<?= '/upload/goods/' . $galerySlide->image ?>">
                                                        <div class="lupeicon notalone"><i class="fa fa-search"></i>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="detailholder">

                                            <div class="portfolio-name"><?= StringHelper::truncate($galerySlide->item, 30) ?></div>
                                            <div class="portfolio-text"><?= StringHelper::truncate($galerySlide->descr, 30) ?></div>
                                            <div class="portfolio-price" style="text-align: center;font-weight: bold;font-size: 20px;color: red"><?= $galerySlide->price . ' Руб.' ?></div>

                                            <div class="portfolio-text"><?= 'Добавлен ' . date('d-m-Y',$galerySlide->created_at) ?></div>
                                            <div class="portfolio-text"><a href="<?= Url::to(['/site/goods-detail','slug' => $galerySlide->slug]) ?>" class="buy-now">Подробнее</a></div>

                                        </div>
                                    </div>
                                </div>

                            <?php endforeach; ?>
                        <?php else: ?>

                            <div class="col-md-4 col-md-offset-5">
                                <p>Список пуст.</p>
                            </div>

                        <?php endif ?>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12 marg50">
            <div class="pagin" style="float:right;">
                <?= LinkPager::widget([
                    'pagination' => $pages,
                    'hideOnSinglePage' => true,
                    'activePageCssClass' => 'current-my',
                    'disabledPageCssClass' => 'current-my-disable',
                    'maxButtonCount' => 5,
                    'prevPageLabel' => '&laquo;',
                    'nextPageLabel' => '&raquo;',

                ]);
                ?>
            </div>
        </div>


    </div>
</div>














