<?php
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\LinkPager;
$Iterator = 0;
?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="welcome">
                <div class="page-name pull-left"><h3>Новости</h3></div>
                <div class="page-link pull-right"><a href="#">Главная</a> / Новости</div>
            </div>
        </div>
    </div>
</div>
<div class="container marg25">
    <div class="row">
        <div class="col-lg-12">
<!--            <div class="portfolioFilter">-->
<!--                <ul>-->
<!--                    <li><a href="#" data-hover="All" data-filter="*" class="current">All</a></li>-->
<!--                    <li><a href="#" data-hover="PHP" data-filter=".php">PHP</a></li>-->
<!--                    <li><a href="#" data-hover="Web Design" data-filter=".web">Web Design</a></li>-->
<!--                    <li><a href="#" data-hover="Photoshop" data-filter=".photoshop">Photoshop</a></li>-->
<!--                    <li><a href="#" data-hover="HTML/CSS" data-filter=".html">HTML/CSS</a></li>-->
<!--                    <li><a href="#" data-hover="Wordpress" data-filter=".wordpress">Wordpress</a></li>-->
<!--                </ul>-->
<!--            </div>-->
            <div class="row marg50">
                <div class="showbiz">
                    <div class="portfolio overflowholder">

                        <? if ($model): ?>
                            <? foreach ($model as $news): ?>
                                <? $Iterator++ ?>

                                 <div class="port-3 photoshop php wordpress">
                                <div class="portfolio-block-in">
                                    <div class="mediaholder">
                                        <div class="mediaholder_innerwrap">
                                            <a href="<?= Url::to(['/site/news-detail','slug'=> $news['slug'] ]) ?>"><img alt="" src="<?= $news['image']; ?>"></a>

                                        </div>
                                    </div>
                                    <div class="detailholder">
                                        <div class="portfolio-text" style="font-weight: bold;font-size: 14px"><?= Html::a(StringHelper::truncate($news['title'],90),Url::to(['/site/news-detail','slug'=> $news['slug']]))  ?></div>

                                    </div>
                                </div>
                            </div>

                            <? endforeach; ?>
                        <? endif; ?>


                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 marg50">
            <div class="pagin" style="float:right;">
                <?= LinkPager::widget([
                    'pagination' => $pages,
                    'hideOnSinglePage' => true,
                    'activePageCssClass' => 'current-my',
                    'disabledPageCssClass' => 'current-my-disable',
                    'maxButtonCount' => 5,
                    //'prevPageLabel' => '&laquo;',
                    //'nextPageLabel' => '&raquo;',

                ]);
                ?>
            </div>
        </div>
    </div>
</div>


<style>
    .showbiz .mediaholder img {

        height: auto;
    }
    .portfolio-text{
        height: 105px;
    }
</style>