<?php
use yii\helpers\Html;
use app\components\RecentGoodsWidget;
use app\components\MonthHelper;

//vd($model);
?>


<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="welcome">
                <div class="page-name pull-left"><h3><?= !empty($model->title) ?  $model->title : false ?></h3></div>
                <div class="page-link pull-right"><a href="/">Главная</a> / <?= !empty($model->title) ?  \yii\helpers\StringHelper::truncate(strip_tags($model->title),30) : false ?></div>
            </div>
        </div>
    </div>
</div>

<div class="container marg25">
    <div class="row">
        <div class="col-lg-9">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
<!--                <ol class="carousel-indicators">-->
<!--                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>-->
<!--                    <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>-->
<!--                    <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>-->
<!--                </ol>-->
                <div class="carousel-inner">
                    <div class="item active">
                        <img alt="" src="<?= !empty($model->image) ? strip_tags($model->image) : false ?>">
                    </div>

                </div>
<!--                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev"><i class="fa fa-arrow-left car_icon"></i></a>-->
<!--                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next"><i class="fa fa-arrow-right car_icon"></i></a>-->
            </div>
        </div>
        <div class="col-lg-3">
            <h3 class="title-in"><span>Описание</span></h3>
            <p class="portfolio-det">

                <i class="fa fa-clock-o icon_foot"></i> Дата: <?= date("d", strtotime($model->created_at)) ?> <?= MonthHelper::setRussianName(date("F", strtotime($model->created_at)),true) ?> <?= date("Y",strtotime($model->created_at)) ?> <?= date("H:i:s",strtotime($model->created_at)) ?><br>
                <i class="fa fa-tags icon_foot"></i> Новости мира<br>
                <i class="fa fa-user icon_foot"></i>  Автор: <a href="#" target="_blank" class="colorend">Admin</a><br>

            </p>
        </div>
        <div class="col-lg-12 marg50">
            <h3 class="title-in"><span>Детали</span></h3>
            <p style="font-size: 15px!important;"><?= !empty($model->content) ?  strip_tags($model->content) : false ?></p>

        </div>
        <div class="col-lg-12">

            <?= RecentGoodsWidget::widget(['template'=>'sidebar']) ?>

        </div>
    </div>
</div>





















