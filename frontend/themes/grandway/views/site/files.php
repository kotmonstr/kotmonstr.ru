<?php
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\widgets\LinkPager;
use yii\helpers\Url;

$Iterator=0;
?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="welcome">
                <div class="page-name pull-left"><h3>Библиотека файлов</h3></div>
                <div class="page-link pull-right"><a href="/">Главная</a> / Библиотека файлов</div>
            </div>
        </div>
    </div>
</div>
<div class="container marg25">
    <div class="row">
        <div class="col-lg-3">
            <h3 class="title-in"><span>Поиск</span></h3>
            <input type="text" name="s" class="form-control searchform" value="">
            <h3 class="title-in"><span>Темы</span></h3>
            <ul class="categories">

                <? if ($modelType): ?>
                    <li><a href="<?= Url::to('/site/files') ?>" class="<?= $currentType == null ? 'active-item' : null ?>"> Все </a></li>
                    <? foreach ($modelType as $type): ?>
                        <li><a href="<?= Url::to(['/site/files','type'=> $type->file_type]) ?>" class="<?= $currentType == $type->file_type ? 'active-item' : null ?>"><?= $type->file_type ?></a></li>
                    <? endforeach ?>
                <? endif; ?>

            </ul>
            <h3 class="title-in"><span>Последние</span></h3>
            <div class="tag_cloud_blog">

                <? if ($modelFilesLast): ?>
                    <? foreach ($modelFilesLast as $file): ?>
                        <a target="_blank" href="<?= Url::to($file->file_path . $file->file_name); ?>" ><?= $file->file_title ?></a>
                    <? endforeach ?>
                <? endif; ?>

            </div>
            <h3 class="title-in"><span>Архив</span></h3>
            <ul class="categories">
                <? if($arrMonths):?>
                    <? foreach ($arrMonths as $date): ?>
                               <li><a class="<?= $currentDate == str_replace(" ","-",$date) ? 'active-item' : null ?>"
                               href="<?= Url::to(['/site/files-date', 'date' => str_replace(" ","-",$date) ]); ?> "><?= $date ?></a></li>
                    <? endforeach; ?>
                <? endif ?>
            </ul>
        </div>
        <div class="col-lg-9">
            <div class="portfolioFilter">
                <ul>
                    <? if ($model): ?>
                        <li><a href="/site/files" class="<?= $currentType == null ? 'current' : null ?>"> Все </a>
                        </li>
                        <? foreach ($modelType as $album): ?>
                            <li><a href="<?= Url::to(['/site/files','type'=> $album->file_type ]) ?>"
                                   data-hover="<?= $album->file_type ?>" data-filter="*"
                                   class="<?= $currentType == $album->file_type ? 'current' : null ?>"><?= $album->file_type ?></a>
                            </li>
                        <? endforeach ?>
                    <? endif; ?>
                </ul>
            </div>
            <div class="row marg50">
                <div class="showbiz">
                    <div class="portfolio overflowholder">

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Наименование</th>
                                <th>Картинка</th>
                                <th>Тип</th>
                                <th>Скачать</th>
                            </tr>
                            </thead>
                            <tbody>

                            <? if (!empty($model)): ?>
                                <? foreach ($model as $file): ?>
                                    <? $Iterator++; ?>

                                    <tr>
                                        <td><?= $Iterator ?></td>
                                        <td title="<?= $file->descr ?>"><?= $file->file_title ?></td>
                                        <td title="<?= $file->descr ?>"><?= Html::img($file->image_path .'/'. $file->image, ['height'=>50]) ?></td>
                                        <td title="<?= $file->descr ?>"><?= $file->file_type ?></td>
                                        <td title="<?= $file->descr ?>"><?= Html::a('скачать',$file->image_path . $file->file_name,['target'=>'_blanck']); ?></td>
                                    </tr>

                                <? endforeach; ?>
                            <? endif ?>

                            </tbody>
                        </table>
                      </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12 marg50">
            <div class="pagin" style="float:right;">
                <?= LinkPager::widget([
                    'pagination' => $pages,
                    'hideOnSinglePage' => true,
                    'activePageCssClass' => 'current-my',
                    'disabledPageCssClass' => 'current-my-disable',
                    'maxButtonCount' => 5,
                    'prevPageLabel' => '&laquo;',
                    'nextPageLabel' => '&raquo;',

                ]);
                ?>
            </div>
        </div>
    </div>
</div>















