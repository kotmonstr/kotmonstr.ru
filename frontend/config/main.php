<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

require(__DIR__ . '/eauth.php');

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        'common\models\Settings',
    ],

    'language' => 'ru-RU',
    'charset' => 'utf-8',
    'timeZone' => 'Europe/Moscow',
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',

            'siteKey' => '6Lc6szIUAAAAACRq3nG-xVfcKGj-V4jtI0FmvTiF', /// local
            'secret' => '6Lc6szIUAAAAAAQ_dDRqmPhLbwvOe0H1L8bBXFKQ',  /// local

            //'siteKey' => '6Lczjx0UAAAAAPVPl7JAyhrilmrTJhCmvQX52WyQ',
            //'secret' => '6Lczjx0UAAAAAI1fKKVN0TCNhqu8N2bGwmCJ9Ivy',
        ],
        'eauth' => $arrEath,
        'i18n' => [
            'translations' => [
                'eauth' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@eauth/messages',
                ],
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            'defaultRoles' => ['user', 'moder', 'admin'], //здесь прописываем роли
            //зададим куда будут сохраняться наши файлы конфигураций RBAC
            'itemFile' => '@common/components/rbac/items.php',
            'assignmentFile' => '@common/components/rbac/assignments.php',
            'ruleFile' => '@common/components/rbac/rules.php',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'htmlLayout' => '@common/mail/layouts/html',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'monstrkot@gmail.com',
                'password' => 'jokers123',
                'port' => '587',
                'encryption' => 'tls',
                'plugins' => [
                    [
                        'class' => 'Swift_Plugins_LoggerPlugin',
                        'constructArgs' => [new Swift_Plugins_Loggers_ArrayLogger],
                    ],
                ],
            ],

        ],

        'request' => [
            'baseUrl' => '',
            //'csrfParam' => '_csrf-token',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [

                'news' => 'site/news',

                'article/index/<category:[\w-]+>' => 'article/index',
                'article/date/<date:[\w-]+>' => 'article/date',
                'article' => 'article/index',
                'article-detail/<slug:[\w-]+>' => 'site/article-detail',

                'site/online/<slug:[\w-]+>' => 'site/online',
                'online' => 'site/online',

                'gallery' => 'site/gallery',
                'gallery/index'=>'gallery/index',
                'gallery/submit-multy' => 'gallery/submit-multy',

                'site/gallery/<slug:[\w-]+>' => 'site/gallery',

                'chat' => 'socket/client',
                'video' => 'site/video',
                'files' => 'site/files',
                'admin' => 'admin/index',
                'goods' => 'site/goods',
                'rss' => 'news/rss',

                'site/gallery/<slug:[\w-]+>' => 'site/gallery',
                'site/video/<slug:[\w-]+>' => 'site/video',
                'site/date/<date:[\w-]+>' => 'site/date',
                'site/video-date/<date:[\w-]+>' => 'site/video-date',
                'site/article-detail/<slug:[\w-]+>' => 'site/article-detail',
                'site/news-detail/<slug:[\w-]+>' => 'site/news-detail',

                'site/goods' => 'goods',
                'site/goods-detail/<slug:[\w-]+>' => 'site/goods-detail',
                'site/goods-date/<date:[\w-]+>' => 'site/goods-date',
                'site/goods/<pod-category:[\w-]+>' => 'site/goods',


                'site/files/<type:[\w-]+>' => 'site/files',
                'site/files-date/<date:[\w-]+>' => 'site/files-date',

                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],
        'LogerClass' => [
            'class' => 'app\components\LogerClass',
        ],
        'view' => [
            'theme' => [

                //'pathMap' => ['@app/views' => '@app/themes/materialize/views'],
                //'baseUrl' => '@web/themes/materialize',
                //'basePath' => '@app/themes/materialize',

                //'pathMap' => ['@app/views' => '@app/themes/saturn/views/'],
                //'baseUrl' => '@web/themes/saturn',
                //'basePath' => '@app/themes/saturn',

                //'pathMap' => ['@app/views' => '@app/themes/meghna/views/'],
                //'baseUrl' => '@web/themes/meghna',
                //'basePath' => '@app/themes/meghna',

                'pathMap' => ['@app/views' => '@app/themes/grandway/views/'],
                'baseUrl' => '@web/themes/grandway',
                'basePath' => '@app/themes/grandway',

                //'pathMap' => ['@app/views' => '@app/themes/carlate/views'],
                //'baseUrl' => '@web/themes/carlate',
                //'basePath' => '@app/themes/carlate',

            ],
        ],
    ],
    'params' => $params,
];
