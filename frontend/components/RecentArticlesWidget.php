<?php
namespace app\components;

use Yii;
use common\models\Article;
use yii\base\Widget;


class RecentArticlesWidget extends Widget
{

    public $model;

    public function init()
    {

        parent::init();
        $this->model = Article::find()
            ->leftJoin('article_category', '`article`.`article_category` = `article_category`.`id`')
            ->where(['article.status'=>1])
            ->where(['article_category.status'=>1])
            ->limit(3)
            ->orderBy('id DESC')
            ->all();

    }

    public function run()
    {

        if ($this->model) {
            echo $this->render('resent-articles' , ['model' => $this->model]);
        }else {
            return false;
        }


    }
}