<?php
use yii\helpers\Url;
//vd($model,false);
?>

<div class="container marg50">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="title"><span>Новинки</span></h3>
            <div class="pagination">
                <a href="" id="clients_left" class="prev"></a>
                <a href="" id="clients_right" class="next"></a>
            </div>
        </div>
    </div>
    <div class="row marg25">
        <div class="col-lg-12">
            <div id="clients" >
                <div class="showbiz" data-left="#clients_left" data-right="#clients_right">
                    <div class="overflowholder">
                        <ul>

                            <? if($model): ?>
                                <? foreach ($model as $article): ?>

                                    <li>
                                        <div class="mediaholder_innerwrap good-item-img">
                                            <a href="<?= Url::to(['/site/good-detail','slug'=> $article->slug]); ?>"><img alt="" src="<?= '/upload/goods/'.$article->image ?>"><div class="reveal_opener_clients show_on_hover"><img alt="" src="<?= '/upload/goods/'.$article->image ?>"></div></a>
                                        </div>
                                    </li>

                                <? endforeach; ?>
                            <? endif; ?>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
  .good-item-img img {
      max-height: 80px;
  }
</style>