<?php $i=0; ?>
<div class="tp-banner-container" style="height:500px;">
    <div class="tp-banner">
        <ul style="display:none;">
            <?php if ($model) : ?>
                <?php foreach ($model as $slide): ?>
                    <?php $i++; ?>

                    <li data-transition="fade" data-slotamount="<?= $i ?>" data-masterspeed="300">
                        <img src="<?= $slide->cloud ? $slide->cloud : '/upload/multy-big/' . $slide->name ?>" alt="" data-bgfit="cover"
                             data-bgposition="center center" data-bgrepeat="no-repeat">
<!--                        <div class="tp-caption largeblackbg skewfromrightshort"-->
<!--                             data-x="12"-->
<!--                             data-y="86"-->
<!--                             data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"-->
<!--                             data-speed="500"-->
<!--                             data-start="800"-->
<!--                             data-easing="Back.easeOut"-->
<!--                             data-endspeed="500"-->
<!--                             data-endeasing="Power4.easeIn"-->
<!--                             data-captionhidden="on"-->
<!--                             style="z-index: 4">--><?//= $slide->text ?>
<!--                        </div>-->

                        <? if($slide->text ): ?>

                            <? if( isset($slide->link) && $slide->link != ''){ ?>
                                    <a href="<?= $slide->link ?>">
                                        <div class="tp-caption medium_bg_asbestos customin customout"
                                             data-x="15"
                                             data-y="350"
                                             data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                                             data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                             data-speed="500"
                                             data-start="1300"
                                             data-easing="Power4.easeOut"
                                             data-endspeed="500"
                                             data-endeasing="Power4.easeIn"
                                             data-captionhidden="on"
                                             style="z-index: 8"><?= $slide->text ?>
                                        </div>
                                    </a>
                                <? } else { ?>
                                <div class="tp-caption medium_bg_asbestos customin customout"
                                     data-x="15"
                                     data-y="350"
                                     data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="500"
                                     data-start="1300"
                                     data-easing="Power4.easeOut"
                                     data-endspeed="500"
                                     data-endeasing="Power4.easeIn"
                                     data-captionhidden="on"
                                     style="z-index: 8"><?= $slide->text ?>
                                </div>
                                <? } ?>

                        <? endif; ?>
<!--                        <div class="tp-caption skewfromrightshort customout"-->
<!--                             data-x="15"-->
<!--                             data-y="280"-->
<!--                             data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"-->
<!--                             data-speed="500"-->
<!--                             data-start="1750"-->
<!--                             data-easing="Back.easeOut"-->
<!--                             data-endspeed="500"-->
<!--                             data-endeasing="Power4.easeIn"-->
<!--                             data-captionhidden="on"-->
<!--                             style="z-index: 9">--><?//= $slide->text ?>
<!--                        </div>-->
<!--                        <div class="tp-caption skewfromleftshort customout"-->
<!--                             data-x="15"-->
<!--                             data-y="350"-->
<!--                             data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"-->
<!--                             data-speed="500"-->
<!--                             data-start="2000"-->
<!--                             data-easing="Back.easeOut"-->
<!--                             data-endspeed="500"-->
<!--                             data-endeasing="Power4.easeIn"-->
<!--                             data-captionhidden="on"-->
<!--                             style="z-index: 10"><a class="btn btn-slider"-->
<!--                                                    href="http://themeforest.net/item/grandway-responsive-html5css3-template/4723385"-->
<!--                                                    target="_blank">--><?//= $slide->text ?><!--</a>-->
<!--                        </div>-->
                       <!-- <div class="tp-caption sfb customout"
                             data-x="right" data-hoffset="-15"
                             data-y="0"
                             data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-speed="800"
                             data-start="2100"
                             data-easing="Back.easeOut"
                             data-endspeed="500"
                             data-endeasing="Power4.easeIn"
                             data-captionhidden="on"
                             style="z-index: 11"><img src="/GrandWay/assets/images/man.png">
                        </div> -->
                    </li>

                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    </div>
</div>

