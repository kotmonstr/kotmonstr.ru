<ul class="contacts images_bg">
    <? if($model->company_name): ?>
        <li class="address">Компания :<?= $model->company_name ?></li>
    <? endif; ?>
    <? if($model->mobile ): ?>
        <li class="phone">Tелефон:  <a href="#"><?= $model->mobile ?></a></li>
    <? endif; ?>
    <? if($model->fax ): ?>
        <li class="fax">Fax: <a href="#"><?= $model->fax ?></a></li>
    <? endif; ?>
    <? if($model->email ): ?>
        <li class="email">E-Mail: <a href="#"><?= $model->email ?></a></li>
    <? endif; ?>
    <? if($model->zip_code ): ?>
        <li class="email">Zip: <a href="#"><?= $model->zip_code ?></a></li>
    <? endif; ?>
    <li class="map"><a href="#">Find us on Google Maps</a></li>
</ul>