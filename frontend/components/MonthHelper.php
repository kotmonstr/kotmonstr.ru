<?php
namespace app\components;

class MonthHelper
{
    const ArrMonths = [
        'January'=>'Январь',
        'February'=>'Февраль',
        'March'=>'Март',
        'April'=>'Апрель',
        'May'=>'Май',
        'June'=>'Июнь',
        'July'=>'Июль',
        'August'=>'Август',
        'September'=>'Сентябрь',
        'October'=>'Октябрь',
        'November'=>'Ноябрь',
        'December'=>'Декабрь',
    ];
    const ArrMonths2 = [
        'Январь'=>'January',
        'Февраль'=>'February',
        'Март'=>'March',
        'Апрель'=>'April',
        'Май'=>'May',
        'Июнь'=>'June',
        'Июль'=>'July',
        'Август'=>'August',
        'Сентябрь'=>'September',
        'Октябрь'=>'October',
        'Ноябрь'=>'November',
        'Декабрь'=>'December',
    ];



    static function setRussianName($engMonthName,$change=false)
    {

        if (array_key_exists($engMonthName, self::ArrMonths)) {
            return $change==true ? str_replace('ь','я',self::ArrMonths[$engMonthName]) : self::ArrMonths[$engMonthName];
        }else{
            return $engMonthName;
        }

    }
    static function setEnglishName($ruMonthName)
    {

        if (array_key_exists($ruMonthName, self::ArrMonths2)) {
            return self::ArrMonths2[$ruMonthName];
        }else{
            return $ruMonthName;
        }

    }
}

?>