<?php

namespace frontend\controllers;

use common\models\Article;
use common\models\ArticleCategory;
use common\models\Files;
use common\models\Gallery;

use common\models\Goods;
use common\models\GoodsCategory;
use common\models\GoodsImage;
use common\models\GoodsPodCategory;

use common\models\Pages;
use common\models\PriceList;
use common\models\Reqvizit;
use common\models\Video;
use common\models\VideoAlbum;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\data\Pagination;
use common\models\Message;
use yii\web\Response;
use common\models\PhotoAlbum;
use app\components\MonthHelper;
use DateTime;
use common\models\News;
use common\models\Comment;
use common\models\User;
use common\models\Canal;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public $menuActiveItem = '';
    public $meta = [];
    const SERVICE = 12;

    public function behaviors()
    {
        return [
            'eauth' => [
                // required to disable csrf validation on OpenID requests
                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                'only' => ['login'],
            ],
        ];
    }
    /**
     * @inheritdoc
     */
//    public function behaviors()
//    {
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'only' => ['logout', 'signup'],
//                'rules' => [
//                    [
//                        'actions' => ['signup'],
//                        'allow' => true,
//                        'roles' => ['?'],
//                    ],
//                    [
//                        'actions' => ['logout'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post','get'],
//                ],
//            ],
//        ];
//    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $this->menuActiveItem = 'index';
        $modelMyWorks = Article::find()->where(['article_category' => 12])->limit(6)->all();

        return $this->render('index',
            [
                'modelMyWorks' => $modelMyWorks,
            ]
        );
    }

    public function actionGallery()
    {
        $arrMonths = [];
        $arrMonthsTest = [];
        $this->layout = 'gallery';
        $this->menuActiveItem = 'image';

        $modelPhotoAlbum = PhotoAlbum::find()
            ->rightJoin('gallery', 'gallery.album_id = photo_album.id')
            ->all();
        $modelPhotoAlbumLast = PhotoAlbum::find()
            ->rightJoin('gallery', 'gallery.album_id = photo_album.id')
            ->orderBy('created_at DESC')
            ->all();

        $query = Gallery::find();
        if (Yii::$app->request->get('slug')) {
            $query = $query
                ->rightJoin('photo_album', 'photo_album.id = gallery.album_id')
                ->where(['status' => 1, 'album_id' => PhotoAlbum::getIdBySlug(Yii::$app->request->get('slug'))]);
        } else {
            $query = $query
                ->where(['status' => 1]);
        }
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => Yii::$app->params['GALLARY_PER_PAGE'] ? Yii::$app->params['GALLARY_PER_PAGE'] : 6]);
        $model = $query->offset($pages->getOffset())
            ->limit($pages->getLimit())
            ->all();

        //months
        $modeMonth = Gallery::find()
            ->select('created_at')
            ->distinct()
            ->all();


        foreach ($modeMonth as $month) {
            //vd($month->created_at,false);
            //vd(date("d-m-y H:i:s",$month->created_at),false);

            $year = date('Y', $month->created_at);
            //vd($year);
            $month = date('F', $month->created_at);
            if (!in_array($month . ' ' . $year, $arrMonthsTest)) {
                $arrMonthsTest[] = $month . ' ' . $year;
                $arrMonths[] = MonthHelper::setRussianName($month) . ' ' . $year;
            }
        }

        return $this->render('gallery',
            [
                'model' => $model,
                'pages' => $pages,
                'arrMonths' => $arrMonths,
                'modelPhotoAlbum' => $modelPhotoAlbum,
                'modelPhotoAlbumLast' => $modelPhotoAlbumLast,
                'currentSlug' => Yii::$app->request->get('slug') ? Yii::$app->request->get('slug') : null,
                'curDate' => isset($curDate) ? $curDate : null,
            ]
        );
    }

    public function actionDate()
    {
        $arrMonths = [];
        $arrMonthsTest = [];
        $this->layout = 'gallery';
        $this->menuActiveItem = 'image';

        $modelPhotoAlbum = PhotoAlbum::find()
            ->rightJoin('gallery', 'gallery.album_id = photo_album.id')
            ->all();

        $modelPhotoAlbumLast = PhotoAlbum::find()
            ->rightJoin('gallery', 'gallery.album_id = photo_album.id')
            ->orderBy('created_at DESC')
            ->all();

        $date = $curDate = Yii::$app->request->get('date');
        $query = Gallery::find();
        if ($date) {
            $pieces = explode("-", $date);
            $month = $pieces[0];
            $year = $pieces[1];

            $d = date("d",strtotime($year . ' ' . MonthHelper::setEnglishName($month)));
            $m = date("m",strtotime($year . ' ' . MonthHelper::setEnglishName($month)));
            $y = date("Y",strtotime($year . ' ' . MonthHelper::setEnglishName($month)));
            $start_date = mktime(0, 0, 0, $m, $d, $y);


            $d = date("d",strtotime($year . ' ' .  MonthHelper::setEnglishName($month). '+1Month'));
            $m = date("m",strtotime($year . ' ' .  MonthHelper::setEnglishName($month). '+1Month'));
            $y = date("Y",strtotime($year . ' ' .  MonthHelper::setEnglishName($month). '+1Month'));
            $end_date = mktime(0, 0, 0, $m, $d, $y);

           // vd($start_date .' | '. $end_date );

            $query = $query->where(['>=', 'created_at', $start_date])
                ->andWhere(['<', 'created_at', $end_date])
                ->andWhere(['status'=> Gallery::VISIBLE])
                ->orderBy('created_at DESC');
        } else {
            $query = $query->orderBy('created_at DESC');
        }

        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => Yii::$app->params['GALLARY_PER_PAGE'] ? Yii::$app->params['GALLARY_PER_PAGE'] : 6]);
        $model = $query->offset($pages->getOffset())
            ->limit($pages->getLimit())
            ->all();

        //months
        $modeMonth = Gallery::find()
            ->select('created_at')
            ->distinct()
            ->all();

        foreach ($modeMonth as $month) {
            $year = date('Y', $month->created_at);
            $month = date('F', $month->created_at);
            if (!in_array($month . ' ' . $year, $arrMonthsTest)) {
                $arrMonthsTest[] = $month . ' ' . $year;
                $arrMonths[] = MonthHelper::setRussianName($month) . ' ' . $year;
            }
        }

        return $this->render('gallery',
            [
                'model' => $model,
                'pages' => $pages,
                'arrMonths' => $arrMonths,
                'modelPhotoAlbum' => $modelPhotoAlbum,
                'modelPhotoAlbumLast' => $modelPhotoAlbumLast,
                'currentSlug' => Yii::$app->request->get('slug') ? Yii::$app->request->get('slug') : null,
                'curDate' => $curDate ? $curDate : null,
            ]
        );
    }

    public function actionArticleDetail()
    {
        $slug = Yii::$app->request->get('slug');
        $this->layout = 'goods';
        $this->menuActiveItem = 'article-ditail';
        $model = Article::find()->where(['slug' => $slug])->one();
        //vd($model);
        $modelArticleLast = Article::find()
            //->where(['article_category' => self::SERVICE])
            ->limit(9)
            ->orderBy('id DESC')
            ->all();

        $modelCategory = ArticleCategory::find()->limit(10)->all();
        $this->meta = $model;

        $modelComment = new Comment;
        $modelComentList = Comment::getMessagesListByBlogId($model->id);
        $messageCount = Comment::getMessagesQuantityByBlogId($model->id);
        if ($modelComment->load(Yii::$app->request->post())) {
            //vd(Yii::$app->request->isAjax);
            // $modelComment->validate();
            //vd($modelComment->getErrors());

            if ($modelComment->validate()) {
                $modelComment->save();

                return $this->refresh();
            }
        }
        Article::UpdateViewCount($model->id);

        return $this->render('article-detail',
            [
                'model' => $model,
                'modelArticleLast' => $modelArticleLast,
                'modelCategory' => $modelCategory,
                'modelComment' => $modelComment,
                'modelComentList' => $modelComentList,
                'messageCount' => $messageCount,
            ]
        );
    }

    public function actionService()
    {
        $this->layout = 'goods';

        $pageSize = 12;
        $query = Article::find()->where(['article_category' => 12]);

        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => Yii::$app->params['SERVICE_PER_PAGE'] ? Yii::$app->params['SERVICE_PER_PAGE'] : 6]);

        $model = $query->offset($pages->offset)
            ->orderBy('created_at DESC')
            ->limit($pages->limit)
            ->all();

        return $this->render('service', [
            'model' => $model,
            'pages' => $pages,
            'pageSize' => $pageSize,

        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        $this->layout = 'goods';

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $serviceName = Yii::$app->getRequest()->getQueryParam('service');
        if (isset($serviceName)) {
            /** @var $eauth \nodge\eauth\ServiceBase */
            $eauth = Yii::$app->get('eauth')->getIdentity($serviceName);
            $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
            $eauth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('site/login'));

            try {
                if ($eauth->authenticate()) {
                    //var_dump($eauth->getIsAuthenticated(), $eauth->getAttributes()); exit;

                    $identity = User::findByEAuth($eauth);
                    Yii::$app->getUser()->login($identity);

                    // special redirect with closing popup window
                    $eauth->redirect();
                } else {
                    // close popup window and redirect to cancelUrl
                    $eauth->cancel();
                }
            } catch (\nodge\eauth\ErrorException $e) {
                // save error to show it later
                Yii::$app->getSession()->setFlash('error', 'EAuthException: ' . $e->getMessage());

                // close popup window and redirect to cancelUrl
//              $eauth->cancel();
                $eauth->redirect($eauth->getCancelUrl());
            }
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $this->layout = 'goods';
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        $this->layout = 'goods';

        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $this->layout = 'goods';

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionGoods()
    {
        $arrMonths = [];
        $arrMonthsTest = [];
        $this->layout = 'gallery';
        $this->menuActiveItem = 'goods';
        $category = Yii::$app->request->get('pod-category');

        $modelGoodsCategory = GoodsCategory::find()
            ->rightJoin('goods_pod_category', '`goods_pod_category`.`category_id` = `goods_category`.`id`')
            ->rightJoin('goods', '`goods_pod_category`.`category_id` = `goods`.`category_id`')
            ->where(['`goods`.`status`' => 1])
            ->with('goods_pod_category')
            ->all();

        $modelGoodLast = Goods::find()
            ->orderBy('created_at DESC')
            ->limit(9)
            ->all();

        $query = Goods::find();
        if ($category) {
            $query = $query
                ->where(['pod_category_id' => GoodsPodCategory::getCatIdBySlug($category), 'status' => 1]);
        } else {
            $query = $query->where(['status' => 1]);
        }
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => Yii::$app->params['GALLARY_PER_PAGE'] ? Yii::$app->params['GALLARY_PER_PAGE'] : 6]);
        $model = $query->offset($pages->getOffset())
            ->limit($pages->getLimit())
            ->orderBy('id DESC')
            ->all();

        //months
        $modeMonth = Goods::find()
            ->select('created_at')
            ->distinct()
            ->all();

        foreach ($modeMonth as $month) {
            $year = date('Y', $month->created_at);
            $month = date('F', $month->created_at);
            if (!in_array($month . ' ' . $year, $arrMonthsTest)) {
                $arrMonthsTest[] = $month . ' ' . $year;
                $arrMonths[] = MonthHelper::setRussianName($month) . ' ' . $year;
            }
        }

        return $this->render('goods',
            [
                'model' => $model,
                'pages' => $pages,
                'arrMonths' => $arrMonths,
                'modelGoodsCategory' => $modelGoodsCategory,
                'modelGoodLast' => $modelGoodLast,
                'currentCategory' => Yii::$app->request->get('pod-category') ? Yii::$app->request->get('pod-category') : null,
                'curDate' => isset($curDate) ? $curDate : null,
            ]
        );
    }

    public function actionGoodsDetail($slug)
    {

        $this->layout = 'goods-detail';
        $this->menuActiveItem = 'goods';

        $model = Goods::find()->where(['slug' => $slug])->one();
        $dopPhotos = GoodsImage::getImagesByGoodsId($model->id);

        return $this->render('goods-detail', [
            'model' => $model,
            'dopPhotos'=> $dopPhotos
        ]);
    }

    public function actionGoodsDate()
    {
        $arrMonths = [];
        $arrMonthsTest = [];
        $this->menuActiveItem = 'goods';
        $this->layout = 'gallery';

        $modelGoodsCategory = GoodsCategory::find()
            ->rightJoin('goods_pod_category', '`goods_pod_category`.`category_id` = `goods_category`.`id`')
            ->rightJoin('goods', '`goods_pod_category`.`category_id` = `goods`.`category_id`')
            ->where(['`goods`.`status`' => 1])
            ->with('goods_pod_category')
            ->all();

        $modelGoodLast = Goods::find()
            ->where(['status' => 1])
            ->orderBy('created_at DESC')
            ->limit(9)
            ->all();

        $date = $curDate = Yii::$app->request->get('date');
        $query = Goods::find();

        if (!empty($date)) {
            $pieces = explode("-", $date);
            $month = $pieces[0];
            $year = $pieces[1];

            $d = date("d",strtotime($year . ' ' . MonthHelper::setEnglishName($month)));
            $m = date("m",strtotime($year . ' ' . MonthHelper::setEnglishName($month)));
            $y = date("Y",strtotime($year . ' ' . MonthHelper::setEnglishName($month)));
            $start_date = mktime(0, 0, 0, $m, $d, $y);


            $d = date("d",strtotime($year . ' ' .  MonthHelper::setEnglishName($month). '+1Month'));
            $m = date("m",strtotime($year . ' ' .  MonthHelper::setEnglishName($month). '+1Month'));
            $y = date("Y",strtotime($year . ' ' .  MonthHelper::setEnglishName($month). '+1Month'));
            $end_date = mktime(0, 0, 0, $m, $d, $y);;


            $query = $query
                ->where(['>=', 'created_at', $start_date])
                ->AndWhere(['<', 'created_at', $end_date])
                ->AndWhere(['status' => 1]);
        } else {
            $query = $query->orderBy('created_at DESC');
        }

        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => Yii::$app->params['GOODS_PER_PAGE'] ? Yii::$app->params['GOODS_PER_PAGE'] : 6]);
        $model = $query->offset($pages->getOffset())
            ->orderBy('created_at DESC')
            ->limit($pages->getLimit())
            ->all();

        //months
        $modeMonth = Goods::find()
            ->select('created_at')
            ->distinct()
            ->all();

        foreach ($modeMonth as $month) {
            $year = date('Y', $month->created_at);
            $month = date('F', $month->created_at);
            if (!in_array($month . ' ' . $year, $arrMonthsTest)) {
                $arrMonthsTest[] = $month . ' ' . $year;
                $arrMonths[] = MonthHelper::setRussianName($month) . ' ' . $year;
            }
        }

        return $this->render('goods',
            [
                'model' => $model,
                'pages' => $pages,
                'arrMonths' => $arrMonths,
                'modelGoodsCategory' => $modelGoodsCategory,
                'modelGoodLast' => $modelGoodLast,
                'curDate' => $curDate ? $curDate : null,
            ]
        );
    }

    public function actionGetPodCatsByCategory()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = Yii::$app->request->post('id');// category_id
        $model = GoodsPodCategory::find()->where(['category_id' => $id])->all();

        return $this->renderAjax('dropdown', ['model' => $model]);
    }

    public function actionContactUs()
    {

        $this->layout = 'goods-detail';

        $model = new Message();
        $modelReqvizit = Reqvizit::find()->one();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $model->save(false);
                Yii::$app->session->setFlash('success', 'Ваше сообщение отправленно.');

                Yii::$app->mailer->compose(
                    [
                        'html' => '@common/mail/letter',
                    ],
                    [
                        'name' => $model->user_name,
                        'message' => $model->message,
                        'subject' => $model->subject,
                        'date' => Yii::$app->formatter->asDatetime($model->created_at),
                    ])
                    ->setFrom('kotkrim+c3b12fc18561@yandex.ru')
                    ->setTo(Yii::$app->params['ADMIN_EMAIL'])
                    ->setSubject($model->subject)
                    ->send();
            }

            return $this->refresh();
        }

        return $this->render('contact-us', [
            'model' => $model,
            'modelReqvizit' => $modelReqvizit,
        ]);
    }

    public function actionPage($slug)
    {
        $this->layout = 'goods-detail';
        $model = Pages::find()->where(['slug' => $slug])->one();

        return $this->render('page', [
            'model' => $model,
        ]);
    }

    public function actionArticle()
    {
        $this->layout = 'goods-detail';

        $modelLast = Article::find()->orderBy('created_at DESC')->limit(10)->all();
        $modelCategory = ArticleCategory::find()->limit(10)->all();
        $category_id = Yii::$app->request->get('category_id');
        $query = Article::find();

        if ($category_id) {
            $query = $query->orderBy('created_at DESC')
                ->where(['article_category' => $category_id]);
        } else {
            $query = $query->orderBy('created_at DESC');
            $model = Article::find()->all();
        }
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => Yii::$app->params['BLOG_PER_PAGE'] ? Yii::$app->params['BLOG_PER_PAGE'] : 12]);

        $model = $query->offset($pages->getOffset())
            ->limit($pages->getLimit())
            ->all();

        $current_month = date('m');
        for ($i = 0; $i >= -5; $i--) {
            $monthList[] = date("F", mktime(0, 0, 0, $current_month + $i, 1, date("Y"))) . ' ' . date("Y");
        }

//vd($monthList);

        return $this->render('article', [
            'model' => $model,
            'pages' => $pages,
            'modelLast' => $modelLast,
            'modelCategory' => $modelCategory,
            'monthList' => $monthList,
        ]);
    }

    public function actionVideo()
    {

        $arrMonths = [];
        $arrMonthsTest = [];
        $this->menuActiveItem = 'video';
        $this->layout = 'gallery';

        $modelVideoAlbum = VideoAlbum::find()
            ->rightJoin('video', 'video.video_album_id = video_album.id')
            ->all();

        $modelVideoAlbumLast = VideoAlbum::find()
            ->rightJoin('video', 'video.video_album_id = video_album.id')
            ->orderBy('created_at DESC')
            ->all();

        $query = Video::find();
        if (Yii::$app->request->get('slug')) {
            $query = $query
                ->rightJoin('video_album', 'video_album.id = video.video_album_id')
                ->where(['video_album_id' => VideoAlbum::getIdBySlug(Yii::$app->request->get('slug'))]);
        }
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => Yii::$app->params['VIDEO_PER_PAGE'] ? Yii::$app->params['VIDEO_PER_PAGE'] : 6]);
        $model = $query->offset($pages->getOffset())
            ->limit($pages->getLimit())
            ->orderBy('created_at DESC')
            ->all();

        //months
        $modeMonth = Video::find()
            ->select('created_at')
            ->distinct()
            ->all();

        foreach ($modeMonth as $month) {
            $year = date('Y', $month->created_at);
            $month = date('F', $month->created_at);
            if (!in_array($month . ' ' . $year, $arrMonthsTest)) {
                $arrMonthsTest[] = $month . ' ' . $year;
                $arrMonths[] = MonthHelper::setRussianName($month) . ' ' . $year;
            }
        }

        return $this->render('video',
            [
                'model' => $model,
                'pages' => $pages,
                'arrMonths' => $arrMonths,
                'modelVideoAlbum' => $modelVideoAlbum,
                'modelVideoAlbumLast' => $modelVideoAlbumLast,
                'currentSlug' => Yii::$app->request->get('slug') ? Yii::$app->request->get('slug') : null,
                'curDate' => isset($curDate) ? $curDate : null,
            ]);
    }

    public function actionVideoDate()
    {

        $arrMonths = [];
        $arrMonthsTest = [];
        $this->menuActiveItem = 'video';
        $this->layout = 'gallery';

        $modelVideoAlbum = VideoAlbum::find()
            ->rightJoin('video', 'video.video_album_id = video_album.id')
            ->all();

        $modelVideoAlbumLast = VideoAlbum::find()
            ->rightJoin('video', 'video.video_album_id = video_album.id')
            ->orderBy('created_at DESC')
            ->all();

        $query = Video::find();

        $date = $curDate = Yii::$app->request->get('date');

        if ($date) {
            $pieces = explode("-", $date);
            $month = $pieces[0];
            $year = $pieces[1];

            $d = date("d",strtotime($year . ' ' . MonthHelper::setEnglishName($month)));
            $m = date("m",strtotime($year . ' ' . MonthHelper::setEnglishName($month)));
            $y = date("Y",strtotime($year . ' ' . MonthHelper::setEnglishName($month)));
            $start_date = mktime(0, 0, 0, $m, $d, $y);


            $d = date("d",strtotime($year . ' ' .  MonthHelper::setEnglishName($month). '+1Month'));
            $m = date("m",strtotime($year . ' ' .  MonthHelper::setEnglishName($month). '+1Month'));
            $y = date("Y",strtotime($year . ' ' .  MonthHelper::setEnglishName($month). '+1Month'));
            $end_date = mktime(0, 0, 0, $m, $d, $y);


            $query = $query->where(['>=', 'created_at', $start_date])
                ->andWhere(['<', 'created_at', $end_date])
                ->orderBy('created_at DESC');
        } else {
            $query = $query->orderBy('created_at DESC');
        }

        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => Yii::$app->params['VIDEO_PER_PAGE'] ? Yii::$app->params['VIDEO_PER_PAGE'] : 6]);
        $model = $query->offset($pages->getOffset())
            ->limit($pages->getLimit())
            ->all();

        //months
        $modeMonth = Video::find()
            ->select('created_at')
            ->distinct()
            ->all();

        foreach ($modeMonth as $month) {
            $year = date('Y', $month->created_at);
            $month = date('F', $month->created_at);
            if (!in_array($month . ' ' . $year, $arrMonthsTest)) {
                $arrMonthsTest[] = $month . ' ' . $year;
                $arrMonths[] = MonthHelper::setRussianName($month) . ' ' . $year;
            }
        }

        return $this->render('video',
            [
                'model' => $model,
                'pages' => $pages,
                'arrMonths' => $arrMonths,
                'modelVideoAlbum' => $modelVideoAlbum,
                'modelVideoAlbumLast' => $modelVideoAlbumLast,
                'currentSlug' => Yii::$app->request->get('slug') ? Yii::$app->request->get('slug') : null,
                'curDate' => $curDate ? $curDate : null,
            ]
        );
    }

    public function actionPriceList()
    {

        $this->layout = 'goods-detail';
        $model = PriceList::find()->all();

        return $this->render('price-list', [
            'model' => $model,
        ]);
    }

    public function actionNews()
    {
        $this->menuActiveItem = 'news';
        $this->layout = 'gallery';
        //$RSS = new RssHelper();
        //$arrNews = $RSS->getUrls();

        $query = News::find();

        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => Yii::$app->params['NEWS_PER_PAGE'] ? Yii::$app->params['NEWS_PER_PAGE'] : 6]);
        $model = $query->offset($pages->getOffset())
            ->limit($pages->getLimit())
            ->orderBy('id DESC')
            ->all();

        return $this->render('news', ['model' => $model, 'pages' => $pages]);
    }

    public function actionFiles()
    {
        $arrMonths = [];
        $arrMonthsTest = [];
        $this->menuActiveItem = 'files';
        $this->layout = 'gallery';

        $modelType = Files::find()
            //->distinct('file_type')
            ->groupBy('file_type')
            ->all();
        //vd($modelType);

        $modelFilesLast = Files::find()
            ->orderBy('created_at DESC')
            ->all();

        $query = Files::find();
        if (Yii::$app->request->get('type')) {
            $query = $query
                ->where(['file_type' => Yii::$app->request->get('type')]);
        }

        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => Yii::$app->params['VIDEO_PER_PAGE'] ? Yii::$app->params['VIDEO_PER_PAGE'] : 6]);
        $model = $query->offset($pages->getOffset())
            ->limit($pages->getLimit())
            ->all();

        //months
        $modeMonth = Files::find()
            ->select('created_at')
            ->distinct()
            ->all();

        foreach ($modeMonth as $month) {
            $year = date('Y', strtotime($month->created_at));
            $month = date('F', strtotime($month->created_at));
            if (!in_array($month . ' ' . $year, $arrMonthsTest)) {
                $arrMonthsTest[] = $month . ' ' . $year;
                $arrMonths[] = MonthHelper::setRussianName($month) . ' ' . $year;
            }
        }

        return $this->render('files',
            [
                'model' => $model,
                'pages' => $pages,
                'arrMonths' => $arrMonths,
                'modelType' => $modelType,
                'modelFilesLast' => $modelFilesLast,
                'currentType' => Yii::$app->request->get('type') ? Yii::$app->request->get('type') : null,
                'currentDate' => isset($currentDate) ? $currentDate : null,

            ]);
    }

    public function actionFilesDate()
    {
        $arrMonths = [];
        $arrMonthsTest = [];
        $this->menuActiveItem = 'files';
        $this->layout = 'gallery';

        $modelType = Files::find()
            ->distinct('file_type')
            ->all();

        $modelFilesLast = Files::find()
            ->orderBy('created_at DESC')
            ->all();

        $date = $currentDate = Yii::$app->request->get('date');
        $query = Files::find();

        if (!empty($date)) {
            $pieces = explode("-", $date);
            $month = $pieces[0];
            $year = $pieces[1];
            $date = new DateTime();
            $date->setTimestamp(strtotime($year . MonthHelper::setEnglishName($month)));
            $start_date = $date->format('Y-m-d H:i:s');
            $date->setTimestamp(strtotime($year . ' ' . MonthHelper::setEnglishName($month) . '+1Month'));
            $end_date = $date->format('Y-m-d H:i:s');
            $query = $query->where(['>=', 'created_at', $start_date])
                ->andWhere(['<', 'created_at', $end_date])
                ->orderBy('created_at DESC');
        } else {
            $query = $query->orderBy('created_at DESC');
        }

        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => Yii::$app->params['VIDEO_PER_PAGE'] ? Yii::$app->params['VIDEO_PER_PAGE'] : 6]);
        $model = $query->offset($pages->getOffset())
            ->limit($pages->getLimit())
            ->all();

        //months
        $modeMonth = Files::find()
            ->select('created_at')
            ->distinct()
            ->all();

        foreach ($modeMonth as $month) {
            $year = date('Y', strtotime($month->created_at));
            $month = date('F', strtotime($month->created_at));
            if (!in_array($month . ' ' . $year, $arrMonthsTest)) {
                $arrMonthsTest[] = $month . ' ' . $year;
                $arrMonths[] = MonthHelper::setRussianName($month) . ' ' . $year;
            }
        }

        return $this->render('files',
            [
                'model' => $model,
                'pages' => $pages,
                'arrMonths' => $arrMonths,
                'modelType' => $modelType,
                'modelFilesLast' => $modelFilesLast,
                'currentType' => Yii::$app->request->get('type') ? Yii::$app->request->get('type') : null,
                'currentDate' => $currentDate ? $currentDate : null,

            ]);
    }

    public function actionNewsDetail()
    {
        $slug = Yii::$app->request->get('slug');
        $this->menuActiveItem = 'news';
        $this->layout = 'gallery';

        $model = News::find()->where(['slug' => $slug])->one();
        $this->meta = $model;

        return $this->render('news-detail', ['model' => $model]);
    }

    public function actionOnline()
    {
        $this->layout = 'gallery';
        $slug = $curDate = Yii::$app->request->get('slug');
        $canalList = Canal::find()->where(['status' => 1])->all();
        $model = Canal::findOne(['slug' => $slug]);

        return $this->render('online', [
            $this->menuActiveItem = 'online',
            'canalList' => $canalList,
            'model' => $model,
            'currentSlug' => Yii::$app->request->get('slug') ? Yii::$app->request->get('slug') : null,
        ]);
    }

    public function actionTest()
    {
        $this->layout = 'gallery';
        return $this->render('test');
    }

    public function actionScript()
    {
        //vd(1);
        $result = shell_exec('/home/kot/www/kotmonstr.loc/scripts/app/start');
        echo $result;
    }
}
