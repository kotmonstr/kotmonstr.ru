<?php

namespace frontend\controllers;

use common\models\ArticleCategory;
use common\models\Blog;
use common\models\Article;
use Yii;
use yii\data\Pagination;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use common\models\Comment;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use common\models\ArticleSearch;
use vova07\imperavi\actions\GetAction;
use yii\web\Response;
use app\components\MonthHelper;
use DateTime;
use yii\db\Expression;

class ArticleController extends CoreController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post', 'search'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['date', 'index', 'create', 'update', 'delete', 'create-image', 'parser-start', 'image-submit', 'create', 'show', 'image-upload', 'images-get', 'upload', 'uploaded', 'view', 'search'],
                        'allow' => true,
                        'roles' => ['@', '?', 'admin'],
                    ],
                    [
                        'actions' => ['index', 'view', 'show', 'views', 'add-news-from-parser'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }


    public $meta = [];
    public $uploudPath = '/web/upload/article';
    public $menuActiveItem = 'article';

    public function actions()
    {
        return [
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => '/frontend/web/upload/article/',
            ],
            'images-get' => [
                'class' => 'vova07\imperavi\actions\GetAction',
                'url' => '/frontend/web/upload/article',
                'type' => GetAction::TYPE_IMAGES,
            ]
        ];
    }

    public function actionIndex()
    {
        $this->layout = 'default';
        $this->menuActiveItem = 'article';

        $catSlug = Yii::$app->request->get('category');
        $query = Article::find();
        if ($catSlug) {
            $query->where(['article_category' => ArticleCategory::getIdBySlug($catSlug), 'status' => ArticleCategory::ACTIVE]);
        } else {
            $query->where(['status' => Article::VISIBLE]);
            $query->leftJoin('article_category', '`article`.`article_category` = `article_category`.`id`')
                ->where(['article.status' => Article::VISIBLE])
                ->where(['article_category.status' => ArticleCategory::ACTIVE]);
        }
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => Yii::$app->params['BLOG_PER_PAGE']]);
        $models = $query
            ->offset($pages->offset)
            ->orderBy('created_at DESC')
            ->limit($pages->limit)
            ->all();

        $modelLastArticle = Article::getLastArticles(5);
        $modeMostWatched = Article::getMostWatched(3);
        $modelArticleCategory = ArticleCategory::getVisibleCategories();
        $arrMonths = Article::getDate();

        return $this->render('index',
            [
                'model' => $models,
                'modelLastArticle' => $modelLastArticle,
                'modeMostWatched' => $modeMostWatched,
                'pages' => $pages,
                'pageSize' => Yii::$app->params['BLOG_PER_PAGE'],
                'modelArticleCategory' => $modelArticleCategory,
                'currentSlug' => $catSlug ? $catSlug : null,
                'arrMonths' => $arrMonths,
                'curDate' => isset($curDate) ? $curDate : null,

            ]);
    }

    public function actionDate()
    {
        $this->layout = 'default';
        $this->menuActiveItem = 'article';

        $date = $curDate = Yii::$app->request->get('date');
        $query = Article::find();

        if ($date) {
            $startDate = $this->getStartDate($date);
            $endDate = $this->getEndDate($date);

            $query = $query->where(['>=', 'created_at', $startDate])
                ->andWhere(['<', 'created_at', $endDate])
                ->leftJoin('article_category', '`article`.`article_category` = `article_category`.`id`')
                ->andWhere(['article.status' => Article::VISIBLE])
                ->andWhere(['article_category.status' => ArticleCategory::ACTIVE])
                ->orderBy('created_at DESC');

        } else {
            $query = $query
                ->leftJoin('article_category', '`article`.`article_category` = `article_category`.`id`')
                ->where(['article.status' => Article::VISIBLE])
                ->where(['article_category.status' => ArticleCategory::ACTIVE])
                ->orderBy('created_at DESC');
        }
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => Yii::$app->params['BLOG_PER_PAGE']]);

        $models = $query->offset($pages->offset)
            ->orderBy('created_at DESC')
            ->limit($pages->limit)
            ->all();

        $modelLastArticle = Article::getLastArticles(5);
        $modeMostWatched = Article::getMostWatched(3);
        $modelArticleCategory = ArticleCategory::getVisibleCategories();
        $arrMonths = Article::getDate();

        return $this->render('index', ['model' => $models,
            'modelLastArticle' => $modelLastArticle,
            'modeMostWatched' => $modeMostWatched,
            'pages' => $pages,
            'pageSize' => Yii::$app->params['BLOG_PER_PAGE'],
            'modelArticleCategory' => $modelArticleCategory,
            'currentSlug' => isset($catSlug) ? $catSlug : null,
            'arrMonths' => $arrMonths,
            'curDate' => $curDate ? $curDate : null
        ]);
    }

    public function actionView()
    {
        $id = Yii::$app->request->get('id');
        $Article = $this->findModel($id);

        return $this->render('view', ['model' => $Article]);
    }

    public function actionShow()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $modelFilter = ArticleCategory::getFilterCategory();

        return $this->render('show', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelFilter' => $modelFilter,
            'arrStatus' => [1 => 'Опубликован', 0 => 'Не опубликован']
        ]);
    }

    public function actionCreate()
    {
        $model = new Article();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['show']);
    }

    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCreateImage()
    {
        FileHelper::createDirectory(Yii::getAlias('@frontend') . '/web/upload/article');
        $model = new Blog();
        $name = date("dmYHis", time());
        if (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');
            $model->file->saveAs('upload/blog/' . $name . '.' . $model->file->extension);
            $full_name = $name . '.' . $model->file->extension;
            return '/upload/article/' . $full_name;
        }
    }

    public function actionViews()
    {
        $slug = Yii::$app->request->get('slug');
        $Article = Article::find()->where(['slug' => $slug])->one();
        if ($Article) {
            $viewsQuantity = (int)$Article->view;
            $Article->view = $viewsQuantity + 1;
            $Article->updateAttributes(['view']);
            $coment_model = Comment::find()->where(['blog_id' => $Article->id])->all();
            $this->meta = $Article;

            return $this->render('views', ['model' => $Article, 'coment_model' => $coment_model]);
        } else {

            return $this->redirect('/site/index');
        }
    }

    public function actionImageSubmit()
    {
        FileHelper::createDirectory(Yii::getAlias('@frontend') . $this->uploudPath);
        $path = Yii::getAlias('@frontend') . $this->uploudPath . '/';
        $model = new Article();
        $name = date("dmYHis", time());
        if (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');
            $model->file->saveAs($path . $name . '.' . $model->file->extension);
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $name . '.' . $model->file->extension;
        }
    }

    public function actionUpload()
    {
        $uploaddir = Yii::getAlias('@frontend') . '/web/upload/article/';
        $file = md5(date('YmdHis')) . '.' . pathinfo(@$_FILES['file']['name'], PATHINFO_EXTENSION);
        if (move_uploaded_file(@$_FILES['file']['tmp_name'], $uploaddir . $file)) {
            $array = array(
                'filelink' => '/upload/article/' . $file
            );
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $array;
    }

    public function actionUploaded()
    {
        $uploaddir = Yii::getAlias('@frontend') . '/web/upload/article';
        $arr = scandir($uploaddir);
        $i = 0;
        foreach ($arr as $key => $val) {
            $i++;
            if ($i > 2) {
                $array['filelink' . $i]['thumb'] = '/upload/article/' . $val;
                $array['filelink' . $i]['image'] = '/upload/article/' . $val;
                $array['filelink' . $i]['title'] = '/upload/article/' . $val;
            }
        }
        $array = stripslashes(json_encode($array));
        echo $array;
    }

    public function actionSearch()
    {
        $request = Yii::$app->request->post('request');
        $query = Article::find();

        $query->where(['like', 'article.title', trim($request)])
            ->with('category')
            ->andWhere(['article.status' => 1]);

        $query->orWhere(['like', 'article.content', trim($request)])
            ->with('category');


        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => Yii::$app->params['BLOG_PER_PAGE']]);

        $model = $query
            ->offset($pages->offset)
            ->orderBy('created_at DESC')
            ->limit($pages->limit)
            ->all();

        $_viewHTML = $this->renderAjax('_ajax-search',
            ['model' => $model,
                'pages' => $pages
            ]);

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $_viewHTML;

    }

    /**
     * @param $date
     * @return false|int
     */
    private function getStartDate($date){
        $pieces = explode("-", $date);
        $month = $pieces[0];
        $year = $pieces[1];

        $d = date("d", strtotime($year . ' ' . MonthHelper::setEnglishName($month)));
        $m = date("m", strtotime($year . ' ' . MonthHelper::setEnglishName($month)));
        $y = date("Y", strtotime($year . ' ' . MonthHelper::setEnglishName($month)));
        $startDate = mktime(0, 0, 0, $m, $d, $y);

        return $startDate;
    }

    /**
     * @param $date
     * @return false|int
     */
    private function getEndDate($date){
        $pieces = explode("-", $date);
        $month = $pieces[0];
        $year = $pieces[1];

        $d = date("d", strtotime($year . ' ' . MonthHelper::setEnglishName($month) . '+1Month'));
        $m = date("m", strtotime($year . ' ' . MonthHelper::setEnglishName($month) . '+1Month'));
        $y = date("Y", strtotime($year . ' ' . MonthHelper::setEnglishName($month) . '+1Month'));
        $endDate = mktime(0, 0, 0, $m, $d, $y);

        return $endDate;
    }


}
