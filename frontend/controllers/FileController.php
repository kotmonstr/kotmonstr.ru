<?php

namespace frontend\controllers;

use Yii;
use common\models\Files;
use common\models\FilesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * FileController implements the CRUD actions for Files model.
 */
class FileController extends CoreController
{
    public $uploudPath = '/web/upload/files/';
    public $imagePath = '/upload/files/';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['date', 'index', 'create', 'update', 'delete', 'create-image', 'parser-start', 'image-submit', 'create', 'show', 'image-upload', 'images-get', 'upload', 'uploaded', 'view'],
                        'allow' => true,
                        'roles' => ['@', '?'],
                    ],
                    [
                        //'actions' => ['index', 'view', 'show', 'views', 'add-news-from-parser'],
                        //'allow' => true,
                        //'roles' => ['@', '?'],
                    ],
                ],
            ],
        ];
    }



    /**
     * Lists all Files models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FilesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Files model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Files model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Files();

        if (Yii::$app->request->isPost) {
            $model->temp_file = UploadedFile::getInstance($model, 'temp_file');
            $model->file_name = uniqid() . '.' . $model->temp_file->extension;
            //vd($model);
            $model->file_path = $this->uploudPath;
            $model->image_path = $this->imagePath;
            if ($model->upload($model->file_name)) {
                     // file is uploaded successfully
            }else{
                $model->validate();
                vd($model->getErrors());
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Files model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost && UploadedFile::getInstance($model, 'temp_file')) {
            $model->temp_file = UploadedFile::getInstance($model, 'temp_file');
            $model->file_name = Yii::getAlias('@frontend') .$model->file_path.$model->temp_file->baseName . '.' . $model->temp_file->extension;
            $model->file_path = $this->uploudPath;
            $model->image_path = $this->imagePath;
            if ($model->upload()) {
                // file is uploaded successfully
            }else{
                $model->validate();
                vd($model->getErrors());
            }
        }





        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Files model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Files model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Files the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Files::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionImageSubmit()
    {
        //тут обычную загрузку картинки
        FileHelper::createDirectory(Yii::getAlias('@frontend') . $this->uploudPath);
        $path = Yii::getAlias('@frontend') . $this->uploudPath . '/';
        //vd($path,false);
        $model = new Files();
        $name = date("dmYHis", time());
        if (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');
            $model->validate();
            //vd($model->getErrors());
            $model->file->saveAs($path . $name. '.' . $model->file->extension);
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $name . '.' . $model->file->extension;
        }
    }
}
