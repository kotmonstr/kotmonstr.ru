$(document).ready(function () {

    $('input[name="search-article"]').on('change', function () {
        var csrf_token = jQuery("meta[name=csrf-token]").attr("content");
        var request = $('input[name="search-article"]').val();
        console.log(request);
        jQuery.ajax({
            url: '/article/search',
            type: 'POST',
            dataType: 'json',
            cache: false,
            data: {
                _csrf: csrf_token,
                request: request
            },
            beforeSend: function (xhr) {
                jpreloader('show');
            },
            success: function (data) {
                jpreloader('hide');


                $('div.col-lg-9').html(data);
            }
        });
    });

});

/* Preloader */
function jpreloader(item) {
    if (item == 'show') {
        $(document.body).append('<div class="back_background jpreloader" style="z-index: 90000;"></div>');
    } else {
        $('.jpreloader').remove();
    }
}