<?php

namespace frontend\assets;

use yii\web\AssetBundle;


class GrandwayAsset extends AssetBundle
{
    public $jsOptions = ['position' => \yii\web\View::POS_END];
    public $basePath = '@webroot/';
    public $baseUrl = '@web/';
    public $css = [
        "GrandWay/assets/css/style.css",
        "GrandWay/assets/css/responsive.css",
        "GrandWay/assets/rs-plugin/css/settings.css",
        "GrandWay/assets/css/navstylechange.css",
        "GrandWay/assets/showbizpro/css/settings-port.css",
        "GrandWay/assets/showbizpro/fancybox/jquery.fancybox.css",
        "GrandWay/assets/showbizpro/fancybox/helpers/jquery.fancybox-thumbs.css",
        "GrandWay/assets/showbizpro/fancybox/helpers/jquery.fancybox-buttons.css",
        "GrandWay/assets/css/custom.css",
        "Particle-js/css/style.css",
    ];

    public $js = [
        "GrandWay/assets/js/jquery-1.20.2.min.js",
        "GrandWay/assets/js/jquery-migrate-1.2.1.min.js",
        "GrandWay/assets/js/modernizr.custom.js",

        "Particle-js/particles.js",
        "Particle-js/js/app.js",

        "GrandWay/assets/rs-plugin/js/jquery.themepunch.plugins.min.js",
        "GrandWay/assets/rs-plugin/js/jquery.themepunch.revolution.min.js",
        "GrandWay/assets/showbizpro/js/jquery.themepunch.showbizpro.js",
        "GrandWay/assets/showbizpro/fancybox/jquery.fancybox.pack.js",
        "GrandWay/assets/showbizpro/fancybox/helpers/jquery.fancybox-thumbs.js",
        "GrandWay/assets/showbizpro/fancybox/helpers/jquery.fancybox-buttons.js",
        "GrandWay/assets/js/waypoints.min.js",
        "GrandWay/assets/js/sticky.js",
        "GrandWay/assets/js/bootstrap.min.js",
        //"GrandWay/assets/js/jquery.tweet.min.js",
        "GrandWay/assets/js/jquery.cycle.all.js",
        "GrandWay/assets/js/retina.js",
        "GrandWay/assets/js/jquery.dlmenu.js",
        "GrandWay/assets/js/main.js",

        //canvas
        "canvas/js/fabric.js",
        "canvas/js/main.js",

        //up/down
        "js/up-down.js"
    ];
    public $depends = [

        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',

    ];
}

