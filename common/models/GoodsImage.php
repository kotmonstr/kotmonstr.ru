<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "goods_image".
 *
 * @property int $id
 * @property int $good_id
 * @property string $name
 * @property int $status_visible
 * @property int $status_main
 * @property string $cloud
 */
class GoodsImage extends \yii\db\ActiveRecord
{

    const STATUS_VISIBLE =1;
    const STATUS_NOT_VISIBLE = 0;
    const STATUS_MAIN = 1;
    const STATUS_SLAVE = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'goods_image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['good_id', 'status_visible', 'status_main'], 'integer'],
            [['name', 'cloud'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'good_id' => 'Good ID',
            'name' => 'Name',
            'status_visible' => 'Status Visible',
            'status_main' => 'Status Main',
            'cloud' => 'Cloud',
        ];
    }

    /**
     * {@inheritdoc}
     * @return GoodsImageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GoodsImageQuery(get_called_class());
    }

    /**
     * @param $id
     * @return array|GoodsImage[]
     */
    public static function getImagesByGoodsId($id)
    {
        $model = self::find()->where(['good_id' => $id,'status_visible'=> self::STATUS_VISIBLE, 'status_main'=>self::STATUS_SLAVE])->all();
        return $model;
    }
}
