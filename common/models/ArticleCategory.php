<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "article_category".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $slug
 * @property string $image
 * @property string $image_path
 */
class ArticleCategory extends \yii\db\ActiveRecord
{
    const ACTIVE = 1;
    const NONACTIVE = 0;

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
            ],
        ];
    }

    public static function tableName()
    {
        return 'article_category';
    }

    public function rules()
    {
        return [
            [['name', 'description'], 'required'],
            [['description'], 'string'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['status'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'description' => 'Описание',
            'slug' => 'Slug',
            'status' => 'Cтатус'
        ];
    }

    public function getArticle()
    {
        return $this->hasMany(Article::className(), ['article_category' => 'id']);
    }


    public static function find()
    {
        return new TemplateQuery(get_called_class());
    }

    public static function is_CatecoryFull($id)
    {
        $countArticles = Article::find()->where(['article_category' => $id])->count();
        return $countArticles > 0 ? false : true;
    }

    public static function getIdBySlug($slug)
    {
        $object = self::find()->where(['slug' => $slug])->one();
        return $object ? $object->id : false;

    }

    public static function getArrStatus()
    {
        return $arr = [
            self::ACTIVE => 'Активный',
            self::NONACTIVE => 'Не активный'
        ];
    }

    public static function getStatusValue($status)
    {

        return $status == self::ACTIVE ? 'Активный' : 'Не активный';
    }

    /**
     * @return array|Template[]|\yii\db\ActiveRecord[]
     */
    public static function getVisibleCategories()
    {
        $model = self::find()
            ->rightJoin('article', '`article`.`article_category` = `article_category`.`id`')
            ->with('article')
            ->where(['article.status' => Article::VISIBLE])
            ->where(['article_category.status' => ArticleCategory::ACTIVE])
            ->all();

        return $model;
    }

    public static function getFilterCategory(){
        $model = [];
        $modelFilterCategory = self::find()->orderBy('id DESC')->all();
        foreach ($modelFilterCategory as $item) {
            $model[$item->name] = $item->name;
        }

        return $model;

    }

}
