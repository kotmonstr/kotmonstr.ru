<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use common\models\Goods;
use Zelenin\yii\behaviors\Slug;

/**
 * This is the model class for table "goods_pod_category".
 *
 * @property integer $id
 * @property string $name
 * @property string $descr
 * @property string $slug
 */
class GoodsPodCategory extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => Slug::className(),
                'attribute' => 'name',
                'ensureUnique' => true,
                'immutable' => true,
                'ensureUnique' => true,
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goods_pod_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'category_id','slug'], 'required'],
            [['description'], 'string'],
            [['name', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'description' => 'Описание',
            'slug' => 'Slug',
            'category_id' => 'Категория'
        ];
    }

    public function getName()
    {
        return $this->name;
    }

    public function getGoods()
    {
        return $this->hasOne(Goods::className(), ['pod_category_id' => 'id']);
    }

    public function getGoods_pod_category()
    {
        return $this->hasMany(GoodsPodCategory::className(), ['id' => 'pod_category_id']);
    }

    public static function getAllByCatId($catId)
    {
        $model = self::find()->where(['category_id' => $catId])->all();
        return $model ? $model : false;
    }

    public static function getCatIdBySlug($slug)
    {
        $model = self::find()->where(['slug' => $slug])->one();
        return $model ? $model->id : false;
    }


}
