<?php

namespace common\models;

use Yii;
use common\models\Article;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property integer $article_id
 * @property string $message
 * @property string $name
 * @property string $email
 * @property string $created_at
 * @property string $updated_at
 */
class Comment extends \yii\db\ActiveRecord
{

    //public $reCaptcha;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_id', 'message','name','email'], 'required'],
            [['article_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            ['email', 'email'],
            [[], \himiklab\yii2\recaptcha\ReCaptchaValidator::className(),'message' => 'Подтвердите, что Вы не робот'],
            //[['reCaptcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator::className(), 'uncheckedMessage' => 'Please confirm that you are not a bot.'],
           // ['reCaptcha', \himiklab\yii2\recaptcha\ReCaptchaValidator::className(), 'message' => 'Подтвердите, что Вы не робот', 'when' => function($model){ return !$model->getErrors() && !Yii::$app->request->isAjax /* !Yii::$app->request->post('ajax')*/; },],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'article_id' => 'Статья',
            'message' => 'Коментарий',
            'name' => 'Имя',
            'email' => 'Email',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlog()
    {
        return $this->hasOne(Blog::className(), ['id' => 'blog_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle0()
    {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }

    public static function getMessagesQuantityByBlogId($blog_id){
        $model= self::find()->where(['article_id'=>$blog_id])->all();
        if($model !== null){
            return count($model);
        }else{
            return 0;
        }
    }
    public static function getMessagesListByBlogId($blog_id){
        $model= self::find()->where(['article_id'=>$blog_id])->all();
        if($model !== null){
            return $model;
        }else{
            return false;
        }
    }
}
