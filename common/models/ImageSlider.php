<?php

namespace common\models;
use Yii;


/**
 * This is the model class for table "{{%images}}".
 *
 * @property string $id
 * @property string $name
 */
class ImageSlider extends \yii\db\ActiveRecord
{
    public $link;
    public $file;
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['text', 'link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%image}}';
    }

  


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'status' => 'Status',
            'text' => 'Text',
            'link' => 'Link',
        ];
    }
    /**
     * return model
     */
    public static function getmodel()
    {
        
        return $model = self::find()->all();
    }
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {

            $fullPath = Yii::getAlias('@frontend')  .'/web/upload/multy-big/'.$this->name;
            if(file_exists($fullPath)) {
                unlink($fullPath);
}
            $fullPath2 = Yii::getAlias('@frontend')  .'/web/upload/multy-thumbs/'.$this->name;
            if(file_exists($fullPath2)) {
                unlink($fullPath2);
}
            return true;
        } else {
            return false;
        }
    }
}
