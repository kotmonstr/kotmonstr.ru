<?php

namespace api\controllers;

use Yii;
use common\models\News;
use common\models\NewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\controllers\CoreController;
use app\components\RssHelper;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends CoreController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],

                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGet()
    {
        $RSS = new RssHelper();
        $arrNews = $RSS->getUrls();

        foreach ($arrNews as $row) {
            $modelBlog = new News();
            $modelBlog->title = $row['Title'];
            $modelBlog->content = $row['Content'];
            $modelBlog->author = Yii::$app->user->identity->username ? Yii::$app->user->identity->username : 'admin';
            $modelBlog->image = $row['Image'];
            $modelBlog->view = 0;
            $dublicate = News::getDublicateByTitle($row['Title']);
            if (!$dublicate) {
                $modelBlog->save();
            }
        }
        Yii::$app->session->setFlash('success', 'Новости получены');
        return $this->redirect(['/admin/index']);
    }

    public function actionGetNewsBefor()
    {
        //vd(1);
        $arrResult = [];
        $id = Yii::$app->request->post('id');

        $model = News::find()->where(['<', 'id', $id])->orderBy('id DESC')->one();
        $arrResult['id'] = isset($model->id) ? $model->id : $id;
        $arrResult['title'] = isset($model->title) ? $model->title : ' ';
        $arrResult['slug'] = isset($model->slug) ? $model->slug : ' ';

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $arrResult;

    }
    public function actionGetNewsAfter()
    {
        //vd(1);
        $arrResult = [];
        $id = Yii::$app->request->post('id');

        $model = News::find()->where(['>', 'id', $id])->orderBy('id ASC')->one();
        $arrResult['id'] = isset($model->id) ? $model->id : $id;
        $arrResult['title'] = isset($model->title) ? $model->title : ' ';
        $arrResult['slug'] = isset($model->slug) ? $model->slug : ' ';

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $arrResult;

    }

    public function actionRss()
    {
        $model = News::find()->all();
        //vd($model);
        Yii::$app->response->format = \yii\web\Response::FORMAT_XML;

        return $model;
    }
}
