<?php
namespace api\controllers;
use common\models\Canal;
use common\models\Files;
use common\models\User;
use common\models\Video;
use Yii;
use yii\web\Controller;
use common\models\Config;
use common\models\Article;
use common\models\Goods;
use common\models\GoodsCategory;
use common\models\ArticleCategory;
use common\models\ImageSlider;
use common\models\GoodsPodCategory;
use common\models\Gallery;
use common\models\Message;
use yii\filters\AccessControl;
use common\models\PhotoAlbum;
use common\models\VideoAlbum;
use common\models\News;
use common\models\Comment;

class CoreController extends Controller {

    public $layout = '/admin';
    public $menuActiveItem = '';

    public $countAllArticles = false;
    public $countAllGoods = false;
    public $countAllGoodsCategory = false;
    public $countAllArticleCategory = false;
    public $countAllSliderFotos = false;
    public $countAllGoodsPodCategory = false;
    public $countAllGalleryPhotos = false;
    public $countAllMessage = false;
    public $countAllVideo = false;
    public $countAllAlbum = false;
    public $countAllVideoAlbum = false;
    public $countAllFiles = false;
    public $countAllNews = false;
    public $countAllUser = false;
    public $countAllCanal = false;
    public $countAllComments = false;

    protected $data = array();

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions'=>['login','error'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['moder','admin'],
                    ],

                ],
            ],

    ];
}




    public function init()
    {
        $items = Config::find()->all();
        foreach ($items as $item){
            if ($item->param)
                $this->data[$item->param] = $item->value === '' ?  $item->default : $item->value;
        }
        $this->getAllCounters();
        parent::init();
    }

    private function getAllCounters(){
        $this->countAllArticles = Article::find()->count();
        $this->countAllGoods = Goods::find()->count();
        $this->countAllGoodsCategory = GoodsCategory::find()->count();
        $this->countAllArticleCategory = ArticleCategory::find()->count();
        $this->countAllSliderFotos = ImageSlider::find()->count();
        $this->countAllGoodsPodCategory = GoodsPodCategory::find()->count();
        $this->countAllGalleryPhotos = Gallery::find()->count();
        $this->countAllMessage = Message::find()->count();
        $this->countAllVideo = Video::find()->count();
        $this->countAllAlbum = PhotoAlbum::find()->count();
        $this->countAllVideoAlbum = VideoAlbum::find()->count();
        $this->countAllFiles = Files::find()->count();
        $this->countAllNews = News::find()->count();
        $this->countAllUser = User::find()->count();
        $this->countAllCanal = Canal::find()->count();
        $this->countAllComments = Comment::find()->count();
    }
}