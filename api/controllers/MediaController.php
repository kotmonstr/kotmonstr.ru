<?php

namespace api\controllers;

use app\components\DirHelper;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class MediaController extends CoreController
{
    public $uploudPath = '/web/upload/';
    public $IN = 'IN';
    public $OUT = 'OUT';
    public $parent = '/';


    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }





    public function actionIndex($url=null,$parent=null,$delete=null,$deleteall=null)
    {
        if($delete){
            if(file_exists(Yii::getAlias('@frontend').$this->uploudPath.$delete)){
                unlink(Yii::getAlias('@frontend').$this->uploudPath.$delete);
            }
        }

        if($url){
            $place = $this->IN;
            $this->parent = $parent;
            $dirList = glob(Yii::getAlias('@frontend').$this->uploudPath .$url. '/'. "*",GLOB_ONLYDIR );
            $fileList = glob(Yii::getAlias('@frontend').$this->uploudPath .$url.'/'. "*.*" );
            $dirListFullInfo = $this->cutPuth($dirList);
            $fileListFullInfo = $this->cutPuth($fileList);
        }else{
            $place = $this->OUT;
            $dirList = glob(Yii::getAlias('@frontend').$this->uploudPath ."*",GLOB_ONLYDIR );
            $fileList = glob(Yii::getAlias('@frontend').$this->uploudPath ."*.*" );
            $dirListFullInfo = $this->cutPuth($dirList);
            $fileListFullInfo = $this->cutPuth($fileList);
        }

        if($deleteall){
            //vd($fileList);
            DirHelper::deleteAllInFoleder($fileList);
            $dirList = glob(Yii::getAlias('@frontend').$this->uploudPath .$url. '/'. "*",GLOB_ONLYDIR );
            $fileList = glob(Yii::getAlias('@frontend').$this->uploudPath .$url.'/'. "*.*" );
            $dirListFullInfo = $this->cutPuth($dirList);
            $fileListFullInfo = $this->cutPuth($fileList);

        }

        return $this->render('index', [
            'url' => $url,
            'parent' => $this->parent,
            'place' => $place,
            'dirList' => $dirListFullInfo,
            'fileList' => $fileListFullInfo
        ]);
    }

    private function cutPuth($arr){
        $result = [];
        $iter = 0;
        foreach ($arr as $item){
            $iter++;
            $result[$iter]['shot']= str_replace(glob(Yii::getAlias('@frontend').$this->uploudPath),"",$item);
            $result[$iter]['full']= $item;
        }
        return $result;
    }
}