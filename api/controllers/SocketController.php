<?php

namespace api\controllers;

use yii\web\Controller;

class SocketController extends Controller
{
    public $menuActiveItem = 'chat';
    public $meta=[];


    public function actionClient()
    {
        $this->layout='default';
        return $this->render('client');
    }


}
