function getNewsBefor(id) {
    var csrf_token = jQuery("meta[name=csrf-token]").attr("content");
    jQuery.ajax({
        url: '/news/get-news-after',
        type: 'POST',
        dataType: 'json',
        cache: false,
        data: {
            _csrf: csrf_token,
            id: id,
        },
        beforeSend: function (xhr) {
           // jpreloader('show');
        },
        success: function (data) {
            //console.log(data);
            //jpreloader('hide');
            //alert(data.id +' | '+ data.state);
            jQuery('#news-one').attr('href','/site/news-detail/'+data.slug);
            jQuery('#news-one').text(data.title);
            jQuery('#prev').attr('onclick','getNewsBefor('+ data.id +')');
            jQuery('#next').attr('onclick','getNewsAfter('+ data.id +')');

            Arrows(data);

        }
    });

}

function getNewsAfter(id) {
    var csrf_token = jQuery("meta[name=csrf-token]").attr("content");
    jQuery.ajax({
        url: '/news/get-news-befor',
        type: 'POST',
        dataType: 'json',
        cache: false,
        data: {
            _csrf: csrf_token,
            id: id,
        },
        beforeSend: function (xhr) {
            // jpreloader('show');
        },
        success: function (data) {
            //console.log(data);
            //jpreloader('hide');
            //alert(data.id +' | '+ data.state);
            jQuery('#news-one').attr('href','/site/news-detail/'+data.slug);
            jQuery('#news-one').text(data.title);
            jQuery('#prev').attr('onclick','getNewsBefor('+ data.id +')');
            jQuery('#next').attr('onclick','getNewsAfter('+ data.id +')');
            Arrows(data);
        }
    });
}

function Arrows(data){
    //console.log(data);
    if(data.title == ' '){
        jQuery('.fa-arrow-left').hide();
    }else{
        jQuery('.fa-arrow-left').show();
    }
}