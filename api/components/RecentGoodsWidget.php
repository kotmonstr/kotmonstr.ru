<?php
namespace app\components;

use Yii;
use common\models\Goods;
use yii\base\Widget;


class RecentGoodsWidget extends Widget
{

    public $model;
    public $template;
    public $limit =6;
    public function init()
    {

        parent::init();

        //$this->model = ImageSlider::find()->all();
        $this->model = Goods::find()->where(['status'=> 1])->limit($this->limit)->orderBy('id DESC')->all();
        //vd($this->model );

    }

    public function run()
    {

        if (isset($this->model) && !empty($this->model) && Yii::$app->params['SHOP_STATUS'] == 1) {
            return $this->render('resent-goods-'.$this->template , ['model' => $this->model]);
        }else {
            return false;
        }


    }
}