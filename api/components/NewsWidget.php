<?php
namespace app\components;

use common\models\Reqvizit;
use Yii;
use yii\base\Widget;
use common\models\News;


class NewsWidget extends Widget
{

    public $model;

    public function init()
    {


        parent::init();

        $this->model = News::find()->orderBy('id DESC')->one();

    }

    public function run()
    {
        if ($this->model && !empty($this->model)) {
            return $this->render('news' , ['model' => $this->model]);
        }else {
            return false;
        }
    }
}