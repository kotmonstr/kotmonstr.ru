<?php
namespace app\components;

use common\models\Reqvizit;
use Yii;
use yii\base\Widget;


class ReqvizitWidget extends Widget
{

    public $model;

    public function init()
    {
        parent::init();
        $this->model = Reqvizit::find()->where(['id'=> 1])->one();
    }

    public function run()
    {
        if ($this->model) {
            echo $this->render('reqvizit' , ['model' => $this->model]);
        }else {
            return false;
        }
    }
}