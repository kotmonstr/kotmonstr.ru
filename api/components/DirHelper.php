<?php

namespace app\components;

use yii\helpers\BaseArrayHelper;
use Yii;
use app\components\PhotoHelper;

class DirHelper extends BaseArrayHelper
{

    public $baseDir = '/web/upload';

    public function Up($url = null)
    {
        $up='';
        $pos = strrpos ($url,"/");
        if($pos){
            $up = substr($url,0,$pos);
        }
        return $up;
    }

    public function getDirs($dirList = null)
    {
        // directoriy
        foreach ($dirList as $dir):
            echo '<tr>';
            echo '<td>';
            echo '<a href="/media/index?url=' .$this->getShotDirName($dir['full']). '">';
            echo '<img src="/images/dir.jpg" alt="" width="25px">';
            echo ' ' . $this->getSuperShotDirName($dir['full']);
            echo '</a>';
            echo '</td>';
            echo "</tr>";
        endforeach;
    }

    private function getShotDirName($dir){
        $item = substr(str_replace(Yii::getAlias('@frontend') . $this->baseDir,"",$dir),1);
        return $item;
    }
    private function getSuperShotDirName($dir){
        $item = substr(str_replace(Yii::getAlias('@frontend') . $this->baseDir,"",$dir),1);
        $pos = strrpos ($item,"/");
        if($pos){
            $item = substr($item,$pos+1);
        }

        return $item;
    }

    public static function deleteAllInFoleder($arr){
        foreach ($arr as $file){
            $pos = strrpos($file, "/");
            $shotFileName = str_replace("/", "", substr($file, $pos));
            if(file_exists($file) && !PhotoHelper::isPhotoDeletable($shotFileName)){
                unlink($file);
            }
        }
    }
}