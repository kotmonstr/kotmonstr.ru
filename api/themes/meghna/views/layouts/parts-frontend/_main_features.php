<section id="main-features">
    <div class="container">
        <div class="row">

            <!-- features item -->
            <div id="features">
                <div class="item">

                    <div class="features-item">

                        <!-- features media -->
                        <div class="col-md-6 feature-media media-wrapper wow fadeInUp" data-wow-duration="500ms">
                            <iframe src="http://player.vimeo.com/video/108018156" allowfullscreen></iframe>
                        </div>
                        <!-- /features media -->

                        <!-- features content -->
                        <div class="col-md-6 feature-desc wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms">
                            <h3>Main Features of Meghna</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, dolores corporis maxime modi amet nisi quod delectus voluptas deleniti facere. Suscipit, modi ex magni quam nesciunt ullam nemo natus soluta!</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, dolores corporis maxime modi amet nisi quod delectus voluptas deleniti facere. Suscipit, modi ex magni quam nesciunt ullam nemo natus soluta!</p>
                            <a href="#" class="btn btn-transparent">Learn More</a>
                            <a href="#" class="btn btn-transparent">Purchase Theme</a>
                        </div>
                        <!-- /features content -->

                    </div>
                </div>

                <div class="item">
                    <div class="features-item">

                        <!-- features media -->
                        <div class="col-md-6 feature-media wow fadeInUp" data-wow-duration="500ms">
                            <img src="/MEGHNA/img/blog/3D-beach-art.jpg" alt="3D Beach Art | Meghna" class="img-responsive">
                        </div>
                        <!-- /features media -->

                        <!-- features content -->
                        <div class="col-md-6 feature-desc wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms">
                            <h3>Main Features of Meghna</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, dolores corporis maxime modi amet nisi quod delectus voluptas deleniti facere. Suscipit, modi ex magni quam nesciunt ullam nemo natus soluta!</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, dolores corporis maxime modi amet nisi quod delectus voluptas deleniti facere. Suscipit, modi ex magni quam nesciunt ullam nemo natus soluta!</p>
                            <a href="#" class="btn btn-transparent">Learn More</a>
                            <a href="#" class="btn btn-transparent">Purchase Theme</a>
                        </div>
                        <!-- /features content -->

                    </div>
                </div>
            </div>
            <!-- /features item -->

        </div> 		<!-- End row -->
    </div>   	<!-- End container -->
</section>   <!-- End section -->