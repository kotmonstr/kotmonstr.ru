
<header id="navigation" class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
        
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
      

       
            <a class="navbar-brand" href="#body">
                <h1 id="logo">
                    <img src="/MEGHNA/img/logo-meghna.png" alt="Meghna" />
                </h1>
            </a>
     
        </div>

     
        <nav class="collapse navbar-collapse navbar-right" role="Navigation">
            <ul id="nav" class="nav navbar-nav">
                <li class="current"><a href="#body">Home</a></li>
                <li><a href="#about">About Us</a></li>
                <li><a href="#services">Services</a></li>
                <li><a href="#showcase">Portfolio</a></li>
                <li><a href="#our-team">Team</a></li>
                <li><a href="#pricing">Pricing</a></li>
                <li><a href="#blog">Blog</a></li>
                <li><a href="#contact-us">Contact</a></li>
            </ul>
        </nav>
 

    </div>
</header>