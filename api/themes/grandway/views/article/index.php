<?php
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\helpers\StringHelper;
use common\models\Comment;
use yii\helpers\Html;
use app\components\MonthHelper;
use common\models\Article;
use frontend\assets\GrandwayAsset;

Yii::$app->formatter->locale = 'ru-RU';
$this->registerJsFile('/js/search-article.js', ['depends'=> GrandwayAsset::className()]);

?>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="welcome">
                <div class="page-name pull-left"><h3>Статьи</h3></div>
                <div class="page-link pull-right"><a href="/">Главная</a> / Статьи</div>
            </div>
        </div>
    </div>
</div>
<div class="container marg25">
    <div class="row">
        <div class="col-lg-3">
            <h3 class="title-in"><span>Поиск</span></h3>
            <input type="text" name="search-article" class="form-control searchform" value="">

            <h3 class="title-in"><span>Темы</span></h3>
            <ul class="categories">

                <? if($modelArticleCategory):?>
                    <? foreach ($modelArticleCategory as $cat): ?>
                        <li>
                            <a class="<?= isset($currentSlug) && $currentSlug == $cat->slug ? 'active-item' : null ?>" href="<?= Url::to(['/article/index', 'category' => $cat->slug]); ?> "><?= $cat->name ?></a>
                        </li>
                    <? endforeach; ?>
                <? endif ?>

            </ul>


            <h3 class="title-in"><span>Последние</span></h3>

            <div class="tag_cloud_blog">

                <? if($modelLastArticle):?>
                    <? foreach ($modelLastArticle as $article): ?>

                            <a href="<?= Url::to(['/site/article-detail', 'slug' => $article->slug]); ?> " title="<?= $article->title ?>"><?= StringHelper::truncate($article->title,20,'...') ?></a>

                    <? endforeach; ?>
                <? endif ?>


            </div>
            <h3 class="title-in"><span>Архив</span></h3>
            <ul class="categories">

                <? if($arrMonths):?>
                    <? foreach ($arrMonths as $date): ?>
                        <li><a href="<?= Url::to(['/article/date', 'date' => str_replace(" ","-",$date) ]); ?>" class="<?= $curDate == str_replace(" ","-",$date) ? 'active-item' : null ?>"><?= $date ?></a></li>
                    <? endforeach; ?>
                <? endif ?>

            </ul>
        </div>
        <div class="col-lg-9">

            <? if ($model): ?>
                <?php foreach ($model as $blog): ?>

                    <div class="post">
                        <div class="medium-5">
                            <div class="post-img"><a href="<?= Url::to(['/site/article-detail','slug'=>$blog->slug]); ?>"><img src="<?= $blog->cloud ? $blog->cloud : $blog->src . '/' . $blog->image ?>" alt=""></a></div>
                        </div>
                        <div class="medium-7">
                            <div class="date-blog">
                                <span class="day-med"><?= date("d", $blog->created_at) ?> <?= MonthHelper::setRussianName(date("F", $blog->created_at),true) ?></span>
                                <span class="month"><?= date("Y") ?></span>
                            </div>
                            <div class="post-content-med">
                                <div class="meta">
                                    <h2><a href="<?= Url::to(['/site/article-detail','slug'=>$blog->slug]); ?>"><?= StringHelper::truncate($blog->title, 63) ?></a></h2>
                                    <span><i class="fa fa-user"></i><a href="<?= Url::to(['/site/article-detail','slug'=>$blog->slug]); ?>"> <?= !empty($blog->author) ? $blog->author : "Admin" ?></a></span>
                                    <span><i class="fa fa-tags"></i><a href="<?= Url::to(['/article/index','category'=> $blog->category->slug ]) ?>"> <?= $blog->category->name ?></a></span>
                                    <span><i class="fa fa-comments"></i><a href="<?= Url::to(['/site/article-detail','slug'=>$blog->slug]); ?>"> <?= Comment::getMessagesQuantityByBlogId($blog->id) > 0 ? Comment::getMessagesQuantityByBlogId($blog->id).' Коментариев' : ' Нет коментариев'; ?></a></span>
                                    <span><i class="fa fa-comments"></i><a href="#"> <?= Article::getViewCount($blog->id) ?> Просмотров</a></span>
                                </div>
                            </div>

                            <p><?=  Html::decode(strip_tags(StringHelper::truncate($blog->content, 366))) ?></p>



                            <div class="read"><a href="<?= Url::to(['/site/article-detail','slug'=>$blog->slug]); ?>">Подробнее</a></div>
                        </div>
                        <div class="lineblog-med"></div>
                    </div>

                <? endforeach; ?>
            <? endif; ?>

            <div class="pagin" style="float:right;">
                <?= LinkPager::widget([
                    'pagination' => $pages,
                    //'hideOnSinglePage' => true,
                    'activePageCssClass' => 'current-my',
                    'disabledPageCssClass' => 'current-my-disable',
                    //'maxButtonCount' => 5,
                    //'prevPageLabel' => '&laquo;',
                    //'nextPageLabel' => '&raquo;',

                                  ]);
                ?>
            </div>



        </div>
    </div>
</div>




