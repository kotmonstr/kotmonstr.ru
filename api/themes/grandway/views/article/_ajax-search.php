<?php
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\helpers\StringHelper;
use common\models\Comment;
use yii\helpers\Html;
use app\components\MonthHelper;
use common\models\Article;
?>



    <? if ($model): ?>
        <?php foreach ($model as $blog): ?>

            <div class="post">
                <div class="medium-5">
                    <div class="post-img"><a href="<?= Url::to(['/site/article-detail','slug'=>$blog->slug]); ?>"><img src="<?= $blog->src . '/' . $blog->image ?>" alt=""></a></div>
                </div>
                <div class="medium-7">
                    <div class="date-blog">
                        <span class="day-med"><?= date("d", strtotime($blog->created_at)) ?> <?= MonthHelper::setRussianName(date("F", strtotime($blog->created_at)),true) ?></span>
                        <span class="month"><?= date("Y") ?></span>
                    </div>
                    <div class="post-content-med">
                        <div class="meta">
                            <h2><a href="<?= Url::to(['/site/article-detail','slug'=>$blog->slug]); ?>"><?= StringHelper::truncate($blog->title, 63) ?></a></h2>
                            <span><i class="fa fa-user"></i><a href="<?= Url::to(['/site/article-detail','slug'=>$blog->slug]); ?>"> <?= !empty($blog->author) ? $blog->author : "Admin" ?></a></span>
                            <span><i class="fa fa-tags"></i><a href="<?= Url::to(['/article/index','category'=> $blog->category->slug ]) ?>"> <?= $blog->category->name ?></a></span>
                            <span><i class="fa fa-comments"></i><a href="<?= Url::to(['/site/article-detail','slug'=>$blog->slug]); ?>"> <?= Comment::getMessagesQuantityByBlogId($blog->id) > 0 ? Comment::getMessagesQuantityByBlogId($blog->id).' Коментариев' : ' Нет коментариев'; ?></a></span>
                            <span><i class="fa fa-comments"></i><a href="#"> <?= Article::getViewCount($blog->id) ?> Просмотров</a></span>
                        </div>
                    </div>

                    <p><?=  Html::decode(strip_tags(StringHelper::truncate($blog->content, 366))) ?></p>



                    <div class="read"><a href="<?= Url::to(['/site/article-detail','slug'=>$blog->slug]); ?>">Подробнее</a></div>
                </div>
                <div class="lineblog-med"></div>
            </div>

        <? endforeach; ?>
    <? else: ?>
            <?= 'Ничего не найдено' ?>
    <? endif; ?>

    <div class="pagin" style="float:right;">
        <?= LinkPager::widget([
            'pagination' => $pages,
            //'hideOnSinglePage' => true,
            'activePageCssClass' => 'current-my',
            'disabledPageCssClass' => 'current-my-disable',
            //'maxButtonCount' => 5,
            //'prevPageLabel' => '&laquo;',
            //'nextPageLabel' => '&raquo;',

        ]);
        ?>
    </div>



