<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\GrandwayAsset;
use common\widgets\Alert;
use app\components\SeoHelper;

GrandwayAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/GrandWay/assets/images/favicon.ico" rel="shortcut icon"/>
    <?= Html::csrfMetaTags() ?>
    <?= SeoHelper::getMeta(); ?>
    <?php $this->head() ?>
</head>
<body>
<div id="particles-js">
</div>
<div id="wrapper">

    <?php $this->beginBody() ?>

    <?= $this->render('parts-frontend/_header'); ?>
    <?= $this->render('parts-frontend/_page_head'); ?>
    <?//= $this->render('parts-frontend/_tp-banner-container'); ?>
    <?//= $this->render('parts-frontend/_container_1'); ?>
    <?//= $this->render('parts-frontend/_container_2'); ?>
    <?//= $this->render('parts-frontend/_container_3'); ?>
    <?//= $this->render('parts-frontend/_posts'); ?>
    <?///= $this->render('parts-frontend/_container_4'); ?>
    <?//= $this->render('parts-frontend/_container_5'); ?>
    <?//= $this->render('parts-frontend/_twitter'); ?>



    <?= $content ?>


    <?= $this->render('parts-frontend/_footer'); ?>
    <?= $this->render('parts-frontend/_footer-bottom'); ?>

    <button onclick="topFunction()" id="myBtn" title="На верх">Top</button>


</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-96404263-1', 'auto');
    ga('send', 'pageview');

</script>
