<header>
    <div class="top_line">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 pull-left"><p>Подписаться на новости: <u><a href="/rss">RSS Feed</a></u></p></div>
                <div class="col-lg-3 pull-left">
                    <p><?= !Yii::$app->user->isGuest ? 'Добро пожаловать '.'<strong>'. Yii::$app->user->identity->username .'</strong>': '' ?>
                    <?= !Yii::$app->user->isGuest ? '| '.'<a href="/site/logout">Выйти</a>': '' ?></p>
                    <?= Yii::$app->user->isGuest ? '|'.'<a href="/site/login">Вход</a>'.' | '.'<a href="/site/signup">Регистрация</a>': '' ?></p>
                </div>
                <div class="col-lg-6 pull-right">
                    <ul class="social-links">
                        <li title="Войти с помощью facebook" class="facebook"><a href="/site/login?service=facebook"><i class="fa fa-facebook ic_soc"></i></a></li>
                        <li title="Войти с помощью twitter" class="twitter"><a href="/site/login?service=twitter"><i class="fa fa-twitter ic_soc"></i></a></li>
                        <!--<li class="dribbble"><a href="#"><i class="fa fa-dribbble ic_soc"></i></a></li>
                        <li class="flickr"><a href="#"><i class="fa fa-flickr ic_soc"></i></a></li>
                        <li class="github"><a href="#"><i class="fa fa-github ic_soc"></i></a></li>
                        <li class="instagram"><a href="#"><i class="fa fa-instagram ic_soc"></i></a></li>
                        <li class="pinterest"><a href="#"><i class="fa fa-pinterest ic_soc"></i></a></li>
                        <li class="youtube"><a href="#"><i class="fa fa-youtube ic_soc"></i></a></li>
                        <!--<li class="apple"><a href="#"><i class="fa fa-apple ic_soc"></i></a></li>
                        <li class="android"><a href="#"><i class="fa fa-android ic_soc"></i></a></li>
                        <li class="bitbucket"><a href="#"><i class="fa fa-bitbucket ic_soc"></i></a></li>
                        <li class="bitbucket-square"><a href="#"><i class="fa fa-bitbucket-square ic_soc"></i></a></li>
                        <li class="bitcoin"><a href="#"><i class="fa fa-bitcoin ic_soc"></i></a></li>
                        <li class="css3"><a href="#"><i class="fa fa-css3 ic_soc"></i></a></li>
                        <li class="dropbox"><a href="#"><i class="fa fa-dropbox ic_soc"></i></a></li>
                        <li class="facebook-square"><a href="#"><i class="fa fa-facebook-square ic_soc"></i></a></li>
                        <li class="foursquare"><a href="#"><i class="fa fa-foursquare ic_soc"></i></a></li>
                        <li class="github-alt"><a href="#"><i class="fa fa-github-alt ic_soc"></i></a></li>
                        <li class="github-square"><a href="#"><i class="fa fa-github-square ic_soc"></i></a></li>
                        <li class="google-plus"><a href="#"><i class="fa fa-google-plus ic_soc"></i></a></li>
                        <li class="google-plus-square"><a href="#"><i class="fa fa-google-plus-square ic_soc"></i></a></li>
                        <li class="html5"><a href="#"><i class="fa fa-html5 ic_soc"></i></a></li>
                        <li class="linkedin"><a href="#"><i class="fa fa-linkedin ic_soc"></i></a></li>
                        <li class="linkedin-square"><a href="#"><i class="fa fa-linkedin-square ic_soc"></i></a></li>
                        <li class="linux"><a href="#"><i class="fa fa-linux ic_soc"></i></a></li>
                        <li class="maxcdn"><a href="#"><i class="fa fa-maxcdn ic_soc"></i></a></li>
                        <li class="pagelines"><a href="#"><i class="fa fa-pagelines ic_soc"></i></a></li>
                        <li class="pinterest-square"><a href="#"><i class="fa fa-pinterest-square ic_soc"></i></a></li>
                        <li class="renren"><a href="#"><i class="fa fa-renren ic_soc"></i></a></li>
                        <li class="skype"><a href="#"><i class="fa fa-skype ic_soc"></i></a></li>
                        <li class="stack-exchange"><a href="#"><i class="fa fa-stack-exchange ic_soc"></i></a></li>
                        <li class="stack-overflow"><a href="#"><i class="fa fa-stack-overflow ic_soc"></i></a></li>
                        <li class="trello"><a href="#"><i class="fa fa-trello ic_soc"></i></a></li>
                        <li class="tumblr"><a href="#"><i class="fa fa-tumblr ic_soc"></i></a></li>
                        <li class="tumblr-square"><a href="#"><i class="fa fa-tumblr-square ic_soc"></i></a></li>
                        <li class="twitter-square"><a href="#"><i class="fa fa-twitter-square ic_soc"></i></a></li>
                        <li class="vimeo-square"><a href="#"><i class="fa fa-vimeo-square ic_soc"></i></a></li> -->
                        <li title="Войти с помощью vk" class="vk"><a href="/site/login?service=vkontakte"><i class="fa fa-vk ic_soc"></i></a></li><!--
                        <li class="weibo"><a href="#"><i class="fa fa-weibo ic_soc"></i></a></li>
                        <li class="windows"><a href="#"><i class="fa fa-windows ic_soc"></i></a></li>
                        <li class="xing"><a href="#"><i class="fa fa-xing ic_soc"></i></a></li>
                        <li class="xing-square"><a href="#"><i class="fa fa-xing-square ic_soc"></i></a></li>
                        <li class="youtube-play"><a href="#"><i class="fa fa-youtube-play ic_soc"></i></a></li>
                        <li class="youtube-square"><a href="#"><i class="fa fa-youtube-square ic_soc"></i></a></li> -->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>