<div class="page_head">
    <div class="nav-container" style="height: auto;">
        <nav>
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 pull-left"><div class="logo"><a href="/">
                                <canvas width="240" height="45" id="c">
                                    Вы это видите, так как Canvas не поддерживается вашим Браузером.
                                </canvas>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-10 pull-right">
                        <div class="menu">
                            <div id="dl-menu" class="dl-menuwrapper">
                                <button class="dl-trigger">Open Menu</button>
                                <ul class="dl-menu">
                                    <li class="<?= $this->context->menuActiveItem == 'index' ? 'current' : null ?>"><a href="/" data-description="Главная">Главная</a></li>
                                    <? if( Yii::$app->params['SHOP_STATUS'] == 1): ?>
                                        <li class="<?= $this->context->menuActiveItem == 'goods' ? 'current' : null ?>"><a href="/goods" data-description="Товары">Товары</a></li>
                                    <? endif; ?>
                                    <li class="<?= $this->context->menuActiveItem == 'article' || $this->context->menuActiveItem == 'article-ditail' ? 'current' : null ?>"><a href="/article" data-description="Стать">Статьи</a></li>
                                    <li class="<?= $this->context->menuActiveItem == 'image' ? 'current' : null ?>"><a href="/gallery" data-description="Картинки">Картинки</a></li>
                                    <li class="<?= $this->context->menuActiveItem == 'video' ? 'current' : null ?>"><a href="/video" data-description="Видео">Видео</a></li>
                                    <li class="<?= $this->context->menuActiveItem == 'news' ? 'current' : null ?>"><a href="/news" data-description="Новости">Новости</a></li>
                                    <li class="<?= $this->context->menuActiveItem == 'online' ? 'current' : null ?>"><a href="/online" data-description="Оn-line TV">On-line TV</a></li>
                                    <li class="<?= $this->context->menuActiveItem == 'files' ? 'current' : null ?>"><a href="/files" data-description="Библиотеа файлов">Библиотеа файлов</a></li>
                                    <li class="<?= $this->context->menuActiveItem == 'chat' ? 'current' : null ?>"><a href="/chat" data-description="Чат">Чат</a></li>
                                    <? if(!Yii::$app->user->isGuest && Yii::$app->user->identity->role == 10): ?><li><a href="/admin" data-description="AdminZone">AdminZone</a></li><? endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</div>