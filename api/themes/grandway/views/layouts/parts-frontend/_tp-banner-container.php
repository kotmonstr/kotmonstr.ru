<div class="tp-banner-container" style="height:500px;">
    <div class="tp-banner" >
        <ul style="display:none;">
            <li data-transition="fade" data-slotamount="7" data-masterspeed="300" >
                <img src="GrandWay/assets/images/slide_bg1.png"  alt=""  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                <div class="tp-caption largeblackbg skewfromrightshort"
                     data-x="12"
                     data-y="86"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="800"
                     data-easing="Back.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 4">Clean responsive design<br>Full retina
                </div>
                <div class="tp-caption medium_bg_asbestos customin customout"
                     data-x="15"
                     data-y="215"
                     data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="1300"
                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 8">Responsive & Retina Ready HTML 5 Template
                </div>
                <div class="tp-caption skewfromrightshort customout"
                     data-x="15"
                     data-y="280"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="1750"
                     data-easing="Back.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 9">Pellentesque luctus ac lorem id luctus. Aenean sagittis magna sit amet purus vehicsula<br>Tristique nunc a felis ultricies, ultrices erat vel, iaculis lacus morbi condimentum
                </div>
                <div class="tp-caption skewfromleftshort customout"
                     data-x="15"
                     data-y="350"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="2000"
                     data-easing="Back.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 10"><a class="btn btn-slider" href="http://themeforest.net/item/grandway-responsive-html5css3-template/4723385" target="_blank">Purchase Now!</a>
                </div>
                <div class="tp-caption sfb customout"
                     data-x="right" data-hoffset="-15"
                     data-y="0"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="800"
                     data-start="2100"
                     data-easing="Back.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 11"><img src="GrandWay/assets/images/man.png">
                </div>
            </li>
            <li data-transition="zoomout" data-slotamount="7" data-masterspeed="1000" >
                <img src="/GrandWay/assets/images/slide_bg2.png"  alt=""  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                <div class="tp-caption lfb customout"
                     data-x="15"
                     data-y="66"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="800"
                     data-start="800"
                     data-easing="Back.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 11"><img src="/GrandWay/assets/images/phone_slide.png">
                </div>
                <div class="tp-caption large_bg_asbestos customin customout"
                     data-x="520"
                     data-y="70"
                     data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="1300"
                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 8">Mobile is the future
                </div>
                <div class="tp-caption white_bg_asbestos skewfromleftshort customout"
                     data-x="520"
                     data-y="126"
                     data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="1700"
                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 8">Fully Responsive & Retina Ready!
                </div>
                <div class="tp-caption skewfromright"
                     data-x="520"
                     data-y="200"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="2000"
                     data-easing="Back.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 9">Pellentesque luctus ac lorem id luctus. Aenean sagittis magna sit amet purus vehicsula<br>Tristique nunc a felis ultricies, ultrices erat vel, iaculis lacus morbi condimentum<br>Nulla non fringilla metus, in tincidunt lacus aenean scelerisque
                </div>
                <div class="tp-caption lft ltb"
                     data-x="520"
                     data-y="300"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="2300"
                     data-easing="Back.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 10"><a class="btn btn-slider" href="http://themeforest.net/item/grandway-responsive-html5css3-template/4723385" target="_blank">Purchase Now!</a>
                </div>
            </li>
            <li data-transition="boxslide" data-slotamount="7" data-masterspeed="500" >
                <img src="assets/images/slide_bg3.jpg"  alt=""  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                <div class="tp-caption large_bg_asbestos customin customout"
                     data-x="15"
                     data-y="70"
                     data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="1300"
                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 8">Best Video Content
                </div>
                <div class="tp-caption white_bg_asbestos skewfromleftshort customout"
                     data-x="15"
                     data-y="126"
                     data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="1700"
                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 8">Youtube or Vimeo
                </div>
                <div class="tp-caption skewfromright"
                     data-x="15"
                     data-y="200"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="2000"
                     data-easing="Back.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 9">Pellentesque luctus ac lorem id luctus. Aenean sagittis magna siton<br>ultricies ultrices erat vel iaculis lacus morbi condimentum
                </div>
                <div class="tp-caption ltb lft"
                     data-x="15"
                     data-y="300"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="2300"
                     data-easing="Back.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 10"><a class="btn btn-slider" href="http://themeforest.net/item/grandway-responsive-html5css3-template/4723385" target="_blank">Purchase Now!</a>
                </div>
                <div class="tp-caption sfb customout"
                     data-x="right" data-hoffset="-183"
                     data-y="100"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="800"
                     data-start="2100"
                     data-easing="Back.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 12"><iframe width="422" height="316" src="http://www.youtube.com/embed/zOyKvfMLOuQ?rel=0" frameborder="0" allowfullscreen style="width:422px; height:316px; border:none;"></iframe>
                </div>
                <div class="tp-caption sfb customout"
                     data-x="right"
                     data-y="50"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="800"
                     data-start="2100"
                     data-easing="Back.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 11"><img src="/GrandWay/assets/images/slide_hand.png">
                </div>
            </li>
        </ul>
    </div>
</div>
<script type="text/javascript">
    var revapi;
    jQuery(document).ready(function() {
        revapi = jQuery('.tp-banner').revolution({
            delay:9000,
            startwidth:1170,
            startheight:500,
            hideThumbs:10,
            forceFullWidth:"off",
        });
    }); //ready
</script>