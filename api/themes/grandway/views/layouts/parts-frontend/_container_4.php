<div class="container marg75">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="title"><span>Clients Testimonials</span></h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 pull-left marg25">
            <div class="testimonial">
                <div class="author"><img src="/GrandWay/assets/images/mike.jpg" alt="" title="" class="img-circle img75"></div>
                <blockquote>
                    <p>Praesent sodales ullamcorper felis, eget rutrum nisi dignissim vel. Nulla non aringilla metus, in tincidunt lacus aenean scelerisque cillum dolore eu rugiat nulla pariatur</p>
                    <cite><b>Georg Brown</b> / SEO Company</cite>
                </blockquote>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 pull-right marg25">
            <div class="testimonial">
                <div class="author"><img src="/GrandWay/assets/images/mila.png" alt="" title=""  class="img-circle img75"></div>
                <blockquote>
                    <p>Nulla non aringilla metus, praesent sodales ullamcorper felis, eget rutrum nisi dignissim vellomnier. Cillum dolore eu rugiat nulla pariatur in tincidunt lacus aenean scelerisque</p>
                    <cite><b>Milena Markovna</b> / Blog Network</cite>
                </blockquote>
            </div>
        </div>
    </div>
</div>