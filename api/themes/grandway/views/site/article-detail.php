<?php
use yii\helpers\Url;
use app\components\MonthHelper;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="welcome">
                <div class="page-name pull-left"><h3><?= $model->title ?></h3></div>
                <div class="page-link pull-right"><a href="#">Статьи</a> / <?= $model->slug ?></div>
            </div>
        </div>
    </div>
</div>
<div class="container marg25">
    <div class="row">
        <div class="col-lg-12">
            <div class="post">
                <div class="post-img">
                    <a href="#">
                        <img src="<?= $model->cloud ? $model->cloud : $model->src . '/' . $model->image ?>" alt="">
                    </a>
                </div>
                <div class="date-blog">
                    <span class="day"><?= date("d", $model->created_at) ?> <?= MonthHelper::setRussianName(date("F", $model->created_at), true) ?></span>
                    <span class="month"><?= date("Y") ?></span>
                </div>
                <div class="post-content">
                    <div class="meta">
                        <h2><a href="#"><?= $model->title ?></a></h2>
                        <span><i class="fa fa-user"></i><a
                                    href="#"> <?= !empty($model->author) ? $model->author : "admin"; ?></a></span>
                        <span><i class="fa fa-tags"></i><a
                                    href="<?= Url::to(['/article/index', 'category' => $model->category->slug]) ?>"> <?= $model->category->name ?> </a></span>
                        <span><i class="fa fa-comment"></i><a href="#"> <?= $messageCount ?> Коментарии</a></span>
                        <span><i class="fa fa-comment"></i><a href="#"> <?= $model->view ?> Просмотров</a></span>
                    </div>
                </div>
                <p class="content-text"><?= $model->content ?></p>
            </div>
            <br>


            <? if ($modelCategory): ?>
                <div class="tag_cloud_blog">

                    <? foreach ($modelCategory as $cat): ?>
                        <a href="<?= Url::to(['/article/index', 'category' => $cat->slug]) ?>"
                           title="<?= $cat->name ?>"><?= $cat->name ?></a>
                    <? endforeach; ?>

                </div>
            <? endif; ?>

            <div class="lineblog-single"></div>
            <div class="sharebox">
                <h4 class="pull-left">Поделится в социальных сетях</h4>
                <ul class="social-links-sec-third pull-right">
                    <li class="facebook"><a href="#"><i class="fa fa-facebook ic_soc"></i></a></li>
                    <li class="twitter"><a href="#"><i class="fa fa-twitter ic_soc"></i></a></li>
                    <li class="dribbble"><a href="#"><i class="fa fa-dribbble ic_soc"></i></a></li>
                    <li class="flickr"><a href="#"><i class="fa fa-flickr ic_soc"></i></a></li>
                    <li class="github"><a href="#"><i class="fa fa-github ic_soc"></i></a></li>
                    <li class="instagram"><a href="#"><i class="fa fa-instagram ic_soc"></i></a></li>
                    <li class="pinterest"><a href="#"><i class="fa fa-pinterest ic_soc"></i></a></li>
                    <li class="youtube"><a href="#"><i class="fa fa-youtube ic_soc"></i></a></li>
                    <li class="android"><a href="#"><i class="fa fa-android ic_soc"></i></a></li>
                    <li class="apple"><a href="#"><i class="fa fa-apple ic_soc"></i></a></li>
                    <li class="bitbucket"><a href="#"><i class="fa fa-bitbucket ic_soc"></i></a></li>
                    <li class="bitbucket-square"><a href="#"><i class="fa fa-bitbucket-square ic_soc"></i></a></li>

                </ul>
            </div>
            <div class="lineblog-single"></div>

            <? if (isset($modelComentList)): ?>

                <span class="comm"><?= $messageCount ?> Коментариев</span>
                <ul class="comment-list">

                    <? foreach ($modelComentList as $comment): ?>

                        <li>
                            <div class="comment-entry">
                                <div class="avatar"><img alt="" src="/GrandWay/assets/images/dan-4.png" class="avatar"
                                                         height="50" width="50"></div>
                                <div class="comment-text">
                                    <div class="author">
                                        <span><?= Html::encode($comment->name) ?></span>
                                        <div class="date"><?= date("d", strtotime($comment->created_at)) ?> <?= MonthHelper::setRussianName(date("F", strtotime($comment->created_at)), true) ?> <?= date("Y", strtotime($comment->created_at)) ?> <?= date("H:i:s", strtotime($comment->created_at)) ?>
                                            · <a class="comment-reply-link" href="#">Ответить</a></div>
                                    </div>
                                    <div class="text">
                                        <p><?= Html::encode($comment->message) ?></p>
                                    </div>
                                </div>
                            </div>
                        </li>


                    <? endforeach; ?>

                </ul>

            <? endif; ?>

            <div class="lineblog-single" style="margin:50px 0;"></div>

            <div id="respond">


                <?php $form = ActiveForm::begin(
                    ['id' => 'commentform',
                        'method'=>'post'
                        //'enableAjaxValidation' => true,
                       // 'enableClientValidation' => true,
                    ]); ?>


                <div class="form-group">

                    <?= $form->field($modelComment, 'name')->textInput(['class' => 'form-control', 'value' => isset(Yii::$app->user->identity->username) ? Yii::$app->user->identity->username : ''])->label('Имя') ?>

                </div>
                <div class="form-group">

                    <?= $form->field($modelComment, 'email')->textInput(['class' => 'form-control'])->label('Email') ?>

                </div>
                <div class="form-group">

                    <?= $form->field($modelComment, 'message')->textarea(['class' => 'form-control', 'rows' => 7])->label('Коментарий') ?>

                </div>

                <div class="form-group">
                    <?= $form->field($modelComment, 'reCaptcha')->widget(\himiklab\yii2\recaptcha\ReCaptcha::className()) ?>
                </div>

                <p class="form-submit">
                    <?= Html::submitButton('Оставить коментарий', ['class' => 'btn btn-default pull-left', 'id' => 'submit_form', 'style' => 'margin-top:15px;']) ?>
                </p>

                <?= $form->field($modelComment, 'article_id')->hiddenInput(['value' => $model->id])->label('', ['class' => 'jj']) ?>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>

<style>

    #wrapper .post p {
        font-size: 17px;

    }

    .control-label {
        float: none !important;
    }
</style>






