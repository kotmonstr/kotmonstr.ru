<?php
use yii\helpers\Html;
use app\components\RecentGoodsWidget;
$Iterator = 0;
?>


<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="welcome">
                <div class="page-name pull-left"><h3><?= $model->item ?></h3></div>
                <div class="page-link pull-right"><a href="/">Главная</a> / <?= $model->item ?></div>
            </div>
        </div>
    </div>
</div>

<div class="container marg25">
    <div class="row">
        <div class="col-lg-9">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <?php if(isset($dopPhotos)): ?>
                        <?php foreach ($dopPhotos as $photo){ ?>
                            <?php $Iterator ++; ?>
                            <div class="item">
                                <li data-target="#carousel-example-generic" data-slide-to="<?= $Iterator ?>" class=""></li>
                            </div>
                        <?php } ?>
                    <?php endif; ?>
                </ol>
                <div class="carousel-inner">
                    <div class="item active">
                        <img alt="" src="<?= '/upload/goods/'.$model->image ?>">
                    </div>

                    <?php if(isset($dopPhotos)): ?>
                        <?php foreach ($dopPhotos as $photo){ ?>
                            <div class="item ">
                                <img alt="" src="<?= '/upload/goods/'.$photo->name ?>">
                            </div>
                        <?php } ?>
                    <?php endif; ?>

                </div>
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev"><i class="fa fa-arrow-left car_icon"></i></a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next"><i class="fa fa-arrow-right car_icon"></i></a>
            </div>
        </div>
        <div class="col-lg-3">
            <h3 class="title-in"><span>Описание</span></h3>
            <p class="portfolio-det">
                <i class="fa fa-group icon_foot"></i> Цена:  <?= ceil($model->price).' Руб. за ' . $model->units ?><br>
                <i class="fa fa-clock-o icon_foot"></i> Date: 21.04.2013;<br>
                <i class="fa fa-tags icon_foot"></i> Categories: Design, Front-end;<br>
                <i class="fa fa-bar-chart-o icon_foot"></i> Skills: HTML, CSS, Web Design;<br>
                <i class="fa fa-user icon_foot"></i>  Author: <a href="#" target="_blank" class="colorend">Dankov</a><br>
                <i class="fa fa-globe icon_foot"></i>  Website: <a href="#" target="_blank" class="colorend">example.com</a><br>
            </p>
        </div>
        <div class="col-lg-12 marg50">
            <h3 class="title-in"><span>Детали</span></h3>
            <p><?= $model->descr; ?></p>
            <?= Html::a($model->pdf,'/upload/pdf/'.$model->pdf) ?>
        </div>
        <div class="col-lg-12">

            <?= RecentGoodsWidget::widget(['template'=>'sidebar']) ?>

        </div>
    </div>
</div>





















