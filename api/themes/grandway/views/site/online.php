<?php
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use common\models\PhotoAlbum;

?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="welcome">
                <div class="page-name pull-left"><h3>Online TV</h3></div>
                <div class="page-link pull-right"><a href="/">Главная</a> / Online</div>
            </div>
        </div>
    </div>
</div>
<div class="container marg25">
    <div class="row" style="min-height: 300px">
        <div class="col-lg-3">
            <h3 class="title-in"><span>Поиск</span></h3>
            <input type="text" name="s" class="form-control searchform" value="">
            <h3 class="title-in"><span>Каналы</span></h3>
            <ul class="categories">
                <?php if ($canalList): ?>
                    <?php foreach ($canalList as $canal): ?>
                        <li>
                            <a class="<?= isset($currentSlug) && $currentSlug == $canal->slug ? 'active-item' : null ?>" href="<?= Url::to(['/site/online', 'slug' => $canal->slug]) ?>"><?= $canal->name ?></a>
                        </li>
                    <?php endforeach ?>
                <?php endif; ?>
            </ul>
        </div>
        <div class="col-lg-9">
            <div class="portfolioFilter">
                <ul>
                    <?php if ($canalList): ?>
                        </li>
                        <?php foreach ($canalList as $album): ?>
                            <li><a href="<?= Url::to(['/site/online', 'slug' => $album->slug]) ?>"
                                   data-hover="<?= $album->name ?>" data-filter="*"
                                   class="<?= $album->slug == $currentSlug ? 'current' : null ?>"><?= $album->name ?>
                                </a>
                            </li>
                        <?php endforeach ?>
                    <?php endif; ?>

                </ul>
            </div>
            <div class="row marg50">
                <div class="showbiz">
                    <div class="portfolio overflowholder">
                        <?php if ($model): ?>
                            <div class="" style="width: 640px;margin: 0px auto">
                                <iframe width="640" height="480" src="https://www.youtube.com/embed/<?= $model->code ?>?autoplay=1" frameborder="0" allowfullscreen></iframe>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
