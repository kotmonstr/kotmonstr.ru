<?php

use frontend\assets\ReactAsset;

$this->registerJsFile('/js/react',['depends'=> ReactAsset::className()]);

?>

<div id="root"></div>
<script type="text/babel">

    ReactDOM.render(
            <h1>Hello, world!</h1>,
        document.getElementById('root')
    );

</script>
