<?php
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use common\models\PhotoAlbum;

?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="welcome">
                <div class="page-name pull-left"><h3>Картинки</h3></div>
                <div class="page-link pull-right"><a href="/">Главная</a> / Картинки</div>
            </div>
        </div>
    </div>
</div>
<div class="container marg25">
    <div class="row">
        <div class="col-lg-3">
            <h3 class="title-in"><span>Поиск</span></h3>
            <input type="text" name="s" class="form-control searchform" value="">
            <h3 class="title-in"><span>Альбомы</span></h3>
            <ul class="categories">

                <?php if ($modelPhotoAlbum): ?>
                    <li><a class="<?= !isset($currentSlug) ? 'active-item' : null ?>" href="/site/gallery"> Все </a>
                    <?php foreach ($modelPhotoAlbum as $album): ?>
                        <li><a class="<?= isset($currentSlug) && $currentSlug == $album->slug ? 'active-item' : null ?>" href="<?= Url::to(['site/gallery', 'slug' => $album->slug]) ?>"><?= $album->name ?></a>
                        </li>
                    <?php endforeach ?>
                <?php endif; ?>

            </ul>

            <h3 class="title-in"><span>Последние Альбомы</span></h3>
            <div class="tag_cloud_blog">

                <?php if ($modelPhotoAlbumLast): ?>
                    <?php foreach ($modelPhotoAlbumLast as $album): ?>
                        <a class="<?= isset($currentSlug) && $currentSlug == $album->slug ? 'active-item-tag' : null ?>" href="<?= Url::to(['site/gallery', 'slug' => $album->slug]) ?>"><?= $album->name ?></a>
                    <? endforeach ?>
                <?php endif; ?>

            </div>
            <h3 class="title-in"><span>Архив</span></h3>
            <ul class="categories">

                <?php if($arrMonths):?>
                    <?php foreach ($arrMonths as $date): ?>
                        <li><a href="<?= Url::to(['site/date', 'date' => str_replace(" ","-",$date) ]); ?>" class="<?= $curDate == str_replace(" ","-",$date) ? 'active-item' : null ?>"><?= $date ?></a></li>
                    <? endforeach; ?>
                <?php endif ?>

            </ul>
        </div>
        <div class="col-lg-9">
            <div class="portfolioFilter">
                <ul>

                    <?php if ($model): ?>
                        <li><a href="/site/gallery" class="<?= !isset($currentSlug) ? 'current' : null ?>"> Все </a></li>
                        <?php foreach ($modelPhotoAlbum as $album): ?>
                            <li><a href="<?= Url::to(['site/gallery', 'slug' => $album->slug]) ?>"
                                   data-hover="<?= $album->name ?>" data-filter="*"
                                   class="<?= $album->slug == $currentSlug ? 'current' : null ?>"><?= $album->name ?>
                                </a>
                            </li>
                        <? endforeach ?>
                    <?php endif; ?>

                </ul>
            </div>
            <div class="row marg50">
                <div class="showbiz">
                    <div class="portfolio overflowholder">

                        <?php if ($model): ?>
                            <?php foreach ($model as $galerySlide): ?>



                                <div class="port-4 photoshop php wordpress">
                                    <div class="portfolio-block-in">
                                        <div class="mediaholder">
                                            <div class="mediaholder_innerwrap">
                                                <a href="#"><?= Html::a(Html::img($galerySlide->cloud ? $galerySlide->cloud : $galerySlide->file_path . $galerySlide->file_name), $galerySlide->cloud ? $galerySlide->cloud : $galerySlide->file_path . $galerySlide->file_name, ['class' => 'fancybox', 'title' => $galerySlide->text, 'rel' => 'fancybox-thumb']) ?></a>
                                                <a class="fancybox" rel="group2" href="<?= $galerySlide->cloud ? $galerySlide->cloud : '/upload/multy-big/'. $galerySlide->file_name ?>">
                                                <div class="hovercover" data-maxopacity="0.85">
<!--                                                    <a href="#">-->
<!--                                                        <div class="linkicon notalone"><i class="fa fa-link"></i></div>-->
<!--                                                    </a>-->

                                                        <div class="lupeicon notalone"><i class="fa fa-search"></i></div>

                                                </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="detailholder">
                                            <div class="portfolio-name"><?= StringHelper::truncate($galerySlide->text, 60); ?></div>
                                            <div class="portfolio-text"><?= PhotoAlbum::getNameById($galerySlide->album_id); ?></div>
                                        </div>
                                    </div>
                                </div>



                            <?php endforeach; ?>
                        <?php else: ?>

                            <div class="col-md-4 col-md-offset-5">
                                <p>Список пуст.</p>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="col-lg-12 marg50">
        <div class="pagin" style="float:right;">
            <?= LinkPager::widget([
                'pagination' => $pages,
                'hideOnSinglePage' => true,
                'activePageCssClass' => 'current-my',
                'disabledPageCssClass' => 'current-my-disable',
                'maxButtonCount' => 5,
                //'prevPageLabel' => '&laquo;',
                //'nextPageLabel' => '&raquo;',
            ]);
            ?>

        </div>
    </div>
</div>
