<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">

                <h1><?= Html::encode($this->title) ?></h1>

                <p>
                    <?= Html::a('Назад', ['index'], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
            'email:email',

            [
                'attribute'=>'Статус',
                'value'=> User::getStatus($model->status)
            ],
            'created_at',
            [
                'attribute'=>'Зарегистрирован',
                'value'=>       Yii::$app->formatter->asDate($model->created_at, 'long')
            ],
            //'updated_at',

            [
                'attribute'=>'Роль',
                'value'=> User::getRole($model->role)
            ],
        ],
    ]) ?>

            </div>
        </div>
    </div>

</section>
