<?php

namespace console\controllers;

use common\models\Gallery;
use common\models\ImageSlider;
use common\models\Article;
use yii\console\Controller;
use Yii;
use Cloudinary;

class CloudController extends Controller
{
    public $access_token = "a977d4d06c54464a8972e583302053eb";

    public function actionGallery()
    {
        echo "Start Gallery..." . PHP_EOL;

        Cloudinary::config([
            "cloud_name" => "kotmonstr",
            "api_key" => "557547449377879",
            "api_secret" => "FqmZhyhHD2NJw3d6VgcHVtvw9_4",
        ]);

        $oldPhoptosFromSite = Gallery::find()->where(['cloud'=>NULL])->all();

        if($oldPhoptosFromSite){
            foreach ($oldPhoptosFromSite as $galery) {
                $oldFile = Yii::getAlias('@frontend/web/') . $galery->file_path . '/' . $galery->file_name;
                $oldFile2 = Yii::getAlias('@frontend/web/') . 'upload/multy-thumbs/' . $galery->file_name;

                $diskClient = Cloudinary\Uploader::upload($oldFile);
                if ($diskClient['url']) {
                    $this->updateGalleryImage($galery->id, $diskClient['url']);
                }
                if (file_exists($oldFile)) {
                    unlink($oldFile);
                    echo "Удален: " . $oldFile . PHP_EOL;
                }
                if (file_exists($oldFile2)) {
                    unlink($oldFile2);
                    echo "Удален: " . $oldFile . PHP_EOL;
                }
            }
        }else{
            echo "Нечего обновлять"  . PHP_EOL;
        }

    }

    public function actionArticle()
    {
        echo "Start Photo..." . PHP_EOL;

        Cloudinary::config([
            "cloud_name" => "kotmonstr",
            "api_key" => "557547449377879",
            "api_secret" => "FqmZhyhHD2NJw3d6VgcHVtvw9_4",
        ]);

        $oldPhoptosFromSite = Article::find()->where(['cloud'=> NULL ])->all();

        if($oldPhoptosFromSite){
            foreach ($oldPhoptosFromSite as $photo) {
                $oldFile = Yii::getAlias('@frontend/web/') . $photo->src. '/'.$photo->image;
                $diskClient = Cloudinary\Uploader::upload($oldFile);
                if ($diskClient['url']) {
                    $this->updateArticleImage($photo->id, $diskClient['url']);
                }
                if (file_exists($oldFile)) {
                    unlink($oldFile);
                    echo "Удален: " . $oldFile . PHP_EOL;
                }
            }
        }else{
            echo "Нечего обновлять"  . PHP_EOL;
        }

    }

    public function actionSlider()
    {
        echo "Start Slider..." . PHP_EOL;

        Cloudinary::config([
            "cloud_name" => "kotmonstr",
            "api_key" => "557547449377879",
            "api_secret" => "FqmZhyhHD2NJw3d6VgcHVtvw9_4",
        ]);

        $oldPhoptosFromSite = ImageSlider::find()->where(['cloud'=> NULL ])->all();

        if($oldPhoptosFromSite){
            foreach ($oldPhoptosFromSite as $photo) {
                $oldFile = Yii::getAlias('@frontend/web/upload/multy-big/') . $photo->name;
                $diskClient = Cloudinary\Uploader::upload($oldFile);
                if ($diskClient['url']) {
                    $this->updateSliderImage($photo->id, $diskClient['url']);
                }
                if (file_exists($oldFile)) {
                    unlink($oldFile);
                    echo "Удален: " . $oldFile . PHP_EOL;
                }
            }
        }else{
            echo "Нечего обновлять"  . PHP_EOL;
        }

    }

    public function updateArticleImage($id, $url)
    {
        $image = Article::find()->where(['id' => $id])->one();
        if($image){
            $image->cloud = $url;
            $image->update();
            echo "Загружен: " . $url . PHP_EOL;
        }else{
            echo "Все фото обновлены"  . PHP_EOL;
        }

    }

    public function updateSliderImage($id, $url)
    {
        $image = ImageSlider::find()->where(['id' => $id])->one();
        if($image){
            $image->cloud = $url;
            $image->update();
            echo "Загружен: " . $url . PHP_EOL;
        }else{
            echo "Все фото обновлены"  . PHP_EOL;
        }

    }

    public function updateGalleryImage($id, $url)
    {
        $image = Gallery::find()->where(['id' => $id])->one();
        if($image){
            $image->cloud = $url;
            $image->update();
            echo "Загружен: " . $url . PHP_EOL;
        }else{
            echo "Все фото обновлены"  . PHP_EOL;
        }

    }
}