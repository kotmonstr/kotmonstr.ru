<?php

namespace console\controllers;

use Yii;

class EmailController extends \yii\console\Controller
{
    public function actionSend()
    {
        echo "send email".PHP_EOL;

        Yii::$app->mailer->compose()
            ->setFrom('from@domain.com')
            ->setTo('monstrkot@gmail.com')
            ->setSubject('Message subject')
            ->setTextBody('Plain text content')
            ->setHtmlBody('<b>HTML content</b>')
            ->send();
    }
}
