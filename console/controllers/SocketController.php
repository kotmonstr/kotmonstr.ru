<?php

namespace console\controllers;

use yii\console\Controller;
use frontend\components\Chat;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

class SocketController extends Controller
{
    public function actionClient()
    {
        $socket = stream_socket_client('tcp://localhost:8181');
        fwrite($socket, 'Привет');
        fclose($socket);
    }

    public function actionServer()
    {
        $server = IoServer::factory(
            new HttpServer(
                new WsServer(
                    new Chat()
                )
            ),
            8181
        );

        $server->run();
    }
}
