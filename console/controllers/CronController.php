<?php
namespace console\controllers;

use app\components\RssHelper;
use Yii;
use yii\console\Controller;
use keltstr\simplehtmldom\SimpleHTMLDom as SHD;
use common\models\News;
use common\models\Video;
use Madcoda\Youtube;

class CronController extends Controller
{
    protected $url = "http://ria.ru";
    protected $arrLinks = [];
    protected $arrNews = [];
    protected $limit = 10;
    protected $Iterator = 0;
    protected $Iterator2 = 0;
    protected $p = '';

    public function actionInit()
    {
    }

    public function actionNews()
    {
        $arrLinks = [];
        echo "Start getting news..." . PHP_EOL;

        //$html = SHD::file_get_html($this->url);

        $curl = curl_init($this->url);
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $html = curl_exec($curl);
        curl_close($curl);

        $html = SHD::str_get_html($html);

        $fileNews = Yii::getAlias('@frontend/runtime/fileNews.txt');

        // Find all links
        foreach ($html->find('a.lenta__item-size') as $link) {
            $this->Iterator++;
            if ($this->Iterator <= $this->limit)
                $arrLinks[] = $link->href;
        }


        // get data
        $Iterator = 0;
        foreach ($arrLinks as $link) {
            $Iterator++;
            $html2 = SHD::file_get_html($link);
            if (!empty($html2) && isset($html2->find('img', 1)->src) ) {
                $arrNews[$Iterator]['Title'] = $html2->find('h1.article__title', 0)->plaintext;
                $arrNews[$Iterator]['Image'] = $html2->find('img', 1)->src;
                $arrNews[$Iterator]['Content'] = $html2->find('.article__text', 0)->plaintext;

            }

        }

        file_put_contents($fileNews, serialize($arrNews));

        foreach ($arrNews as $row) {

            $modelBlog = new News();
            $modelBlog->title = $row['Title'];
            $modelBlog->content = $row['Content'];
            $modelBlog->author = 'admin';
            $modelBlog->image = $row['Image'];
            $modelBlog->view = 0;
            $dublicate = News::getDublicateByTitle($row['Title']);
            if (!$dublicate && !empty( $modelBlog->image && $modelBlog->image != 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==')) {
                //echo $modelBlog->image .PHP_EOL;
                $this->Iterator2++;
                $modelBlog->save();
            }
        }

        echo "Finish...success, was added ". $this->Iterator2 ." news". PHP_EOL;
    }

    public function actionRemoveFalseVideo()
    {
        $model = Video::find()->all();
        $modelCount = Video::find()->count();
        $youtube = new Youtube(array('key' => 'AIzaSyBU4vsvP20CYdFuibdgTMOaZ10vt7JxV5c'));

        echo "Was find: ". $modelCount .  " videos".PHP_EOL;

        foreach ($model as $video){
            $videoOne = $youtube->getVideoInfo($video->youtube_id);
            if($videoOne == false){
                //vd(1,false);
                echo "Delete: ". $video->id. PHP_EOL;
                Video::findOne($video->id)->delete();
            }
            //vd(2,false);
        }
    }
}