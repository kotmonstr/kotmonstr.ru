<?php
namespace console\controllers;

use Yii;


class NakrutiController extends \yii\console\Controller
{
    function init()
    {

        define('DS', DIRECTORY_SEPARATOR);
        ini_set('display_errors', 'On');
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '128M');
        //Main::defineArrProxi();
    }

    public function actionUp($url, $iterator)
    {
        $this->getNewProxi();

        for ($i = 0; $i < $iterator; $i++) {
           $this->Curl($url);
        }


    }
    public function actionYoutube($url, $iterator)
    {
        $this->getNewProxi();

        for ($i = 0; $i < $iterator; $i++) {
           $this->CurlYoutube($url);
        }


    }

    private function Curl($url)
    {
        //echo "#level up.. " . $url, PHP_EOL;

        $arrAgent = file(Yii::getAlias('@console') . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'useragent_list.txt');
        $agentKey = array_rand($arrAgent, 1);
        $agent = trim($arrAgent[$agentKey]);

        $arrProxi = file(Yii::getAlias('@console') . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'proxy_list.txt');
        $ProxiKey = array_rand($arrProxi, 1);
        $proxiMOD = explode(":", trim($arrProxi[$ProxiKey]));

        $arrUrllist = file(Yii::getAlias('@console') . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'url_list_kotmonstr.txt');

        $Key = array_rand($arrUrllist, 1);
        $url = trim($arrUrllist[$Key]);
        //vd($url);

        // Платные прокси
//            $arrProxi = [];
//            for ($i = 1; $i <= 25; $i++) {
//                $arrProxi[$i] = '144.76.33.130:120' . $i;
//
//            }
//            $x = $arrProxi[rand(1, 25)];
//            $proxiMOD = explode(":",trim($x));


        echo "#level up.. " . $url, PHP_EOL;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        //curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
        curl_setopt($ch, CURLOPT_PROXY, $proxiMOD[0]);
        curl_setopt($ch, CURLOPT_PROXYPORT, $proxiMOD[1]);
        //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'GET');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $output = curl_exec($ch);
        if ($output === FALSE) {
            echo $proxiMOD[0] . ':' . $proxiMOD[1] . ' -' . curl_error($ch), PHP_EOL;
        } else {
            echo $proxiMOD[0] . ':' . $proxiMOD[1] . ' -ok', PHP_EOL;
        }
        curl_close($ch);
        //print_r($output);
        //return $output;
    }
    private function CurlYoutube($url)
    {
        //echo "#level up.. " . $url, PHP_EOL;

        $arrAgent = file(Yii::getAlias('@console') . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'useragent_list.txt');
        $agentKey = array_rand($arrAgent, 1);
        $agent = trim($arrAgent[$agentKey]);

        $arrProxi = file(Yii::getAlias('@console') . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'proxy_list.txt');
        $ProxiKey = array_rand($arrProxi, 1);
        $proxiMOD = explode(":", trim($arrProxi[$ProxiKey]));

        $arrUrllist = file(Yii::getAlias('@console') . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'url_list_kotmonstr.txt');

        $Key = array_rand($arrUrllist, 1);
        //$url = trim($arrUrllist[$Key]);
        //vd($url);

        // Платные прокси
//            $arrProxi = [];
//            for ($i = 1; $i <= 25; $i++) {
//                $arrProxi[$i] = '144.76.33.130:120' . $i;
//
//            }
//            $x = $arrProxi[rand(1, 25)];
//            $proxiMOD = explode(":",trim($x));


        echo "#level up youtube.. " . $url, PHP_EOL;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        //curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
        curl_setopt($ch, CURLOPT_PROXY, $proxiMOD[0]);
        curl_setopt($ch, CURLOPT_PROXYPORT, $proxiMOD[1]);
        //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'GET');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $output = curl_exec($ch);
        if ($output === FALSE) {
            echo $proxiMOD[0] . ':' . $proxiMOD[1] . ' -' . curl_error($ch), PHP_EOL;
        } else {
            echo $proxiMOD[0] . ':' . $proxiMOD[1] . ' -ok', PHP_EOL;
        }
        curl_close($ch);
        //print_r($output);
        //return $output;
    }

    private function getNewProxi(){
        $arrProxi = '';
        $file = Yii::getAlias('@console') . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'proxy_list.txt';
        $listProxi = file_get_contents('http://randgate.wecandevelopit.com/1000/100');
        $arrListProxi = json_decode($listProxi);
        foreach ($arrListProxi as $proxi) {
            $arrProxi .= $proxi->ip . ':' . $proxi->port . "\n";
            $arrProxiA[]=$proxi->ip . ':' . $proxi->port;
        }
        file_put_contents($file, $arrProxi);
    }

}