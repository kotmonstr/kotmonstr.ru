<?php

use yii\db\Migration;

/*
1 id int(11) Нет Нет AUTO_INCREMENT
2 username varchar(255) utf8_unicode_ci Нет Нет
3 auth_key varchar(32) utf8_unicode_ci Нет Нет
4 password_hash varchar(255) utf8_unicode_ci Нет Нет
5 password_reset_token varchar(255) utf8_unicode_ci Да NULL
6 email varchar(255) utf8_unicode_ci Нет Нет
7 status smallint(6) Нет 10
8 created_at int(11) Нет Нет
9 updated_at int(11) Нет Нет
10 role int(11) Да NULL
11 password varchar(255) utf8_unicode_ci Нет Нет
 */

class m130524_201441_create_user_table extends Migration
{
    const TABLE = '{{%user}}';
    const ADMIN_USERNAME = 'kot';
    const ADMIN_AUTH_KEY = '9YYCFRypPST6BhEH38qPEdPeM5PP-8n_';
    const ADMIN_PASSWORD_BASH = '$2y$13$mNR0W8moTPNLigIEkyjLGursn8rzdc2usMZqQYEbBblxIq/rs09p.';
    const ADMIN_PASWORD_RESET = NULL;
    const ADMIN_EMAIL = 'kotmonstr@i.ua';
    const ADMIN_STATUS = 10;
    const ADMIN_CREATED_AT = 1473684005;
    const ADMIN_UPDATED_AT = 1473684005;
    const ADMIN_ROLE = 10;
    const ADMIN_PASWORD = "";

    public function up()
    {
        $this->createTable(self::TABLE, [
            'id' => $this->primaryKey()->unsigned(),
            'username' => $this->string(255),
            'auth_key' => $this->string(32),
            'password_hash' => $this->string(255),
            'password_reset_token' => $this->string(255),
            'email' => $this->string(255),
            'status' => $this->smallInteger(6),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'role' => $this->integer(11),
            'password' => $this->string(255),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->insert(self::TABLE, [
            'username' => self::ADMIN_USERNAME,
            'auth_key' => self::ADMIN_AUTH_KEY,
            'password_hash' => self::ADMIN_PASSWORD_BASH,
            'password_reset_token' => self::ADMIN_PASWORD_RESET,
            'email' => self::ADMIN_EMAIL,
            'status' => self::ADMIN_STATUS,
            'created_at' => self::ADMIN_CREATED_AT,
            'updated_at' => self::ADMIN_UPDATED_AT,
            'role' => self::ADMIN_ROLE,
            'password' => self::ADMIN_PASWORD,
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE);
    }
}
