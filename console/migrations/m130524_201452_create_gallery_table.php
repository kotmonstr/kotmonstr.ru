<?php

use yii\db\Migration;

/*
 * id int(11) Нет Нет AUTO_INCREMENT
2 file_name varchar(255) utf8_general_ci Да NULL
3 file_path varchar(255) utf8_general_ci Да NULL
4 created_at int(11) Да NULL
5 updated_at int(11) Да NULL
6 status int(4) Да NULL
7 text text utf8_general_ci Да NULL
8 album_id int(11) Да NULL
9 cloud varchar(255) utf8_general_ci Да NULL
 */

class m130524_201452_create_gallery_table extends Migration
{
    const TABLE = '{{gallery}}';

    public function up()
    {
        $this->createTable(self::TABLE , [
            'id' => $this->primaryKey()->unsigned(),
            'file_name' => $this->string(255),
            'file_path' => $this->string(255),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'status' => $this->smallInteger(4),
            'text' => $this->text(),
            'album_id' => $this->integer(11),
            'cloud' => $this->string(255),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    public function down(){
        $this->dropTable(self::TABLE );
    }
}
