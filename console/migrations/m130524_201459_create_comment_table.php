<?php

use yii\db\Migration;

/*
 * @property integer $id
 * @property integer $article_id
 * @property string $message
 * @property string $name
 * @property string $email
 * @property string $created_at
 * @property string $updated_at
 */



class m130524_201459_create_comment_table extends Migration
{
    const TABLE = '{{comment}}';

    public function up()
    {
        $this->createTable(self::TABLE , [
            'id' => $this->primaryKey()->unsigned(),
            'article_id' =>  $this->integer(11),
            'message' => $this->text(),
            'name' => $this->string(255),
            'updated_at' => $this->integer(11),
            'created_at' => $this->integer(11),
            'email' =>  $this->string(25),

        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    public function down(){
        $this->dropTable(self::TABLE );
    }
}
