<?php

use yii\db\Migration;

/*
 * id int(11) Нет Нет AUTO_INCREMENT
2 company_name varchar(100) utf8_general_ci Нет Нет
3 country varchar(100) utf8_general_ci Нет Нет
4 address varchar(255) utf8_general_ci Нет Нет
5 mobile varchar(100) utf8_general_ci Нет Нет
6 fax varchar(100) utf8_general_ci Нет Нет
7 email varchar(255) utf8_general_ci Нет Нет
8 schet varchar(255) utf8_general_ci Нет Нет
9 inn varchar(255) utf8_general_ci Нет Нет
10 zip_code int(11) Нет Нет
 */

class m130524_201449_create_reqvizit_table extends Migration
{
    const TABLE = '{{reqvizit}}';

    public function up()
    {
        $this->createTable(self::TABLE , [
            'id' => $this->primaryKey()->unsigned(),
            'company_name' => $this->string(255),
            'country' => $this->string(255),
            'address' => $this->string(255),
            'mobile' => $this->integer(11),
            'fax' => $this->string(255),
            'email' =>  $this->string(255),
            'schet' => $this->string(255),
            'inn' =>  $this->string(255),
            'zip_code' => $this->string(255),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    public function down(){
        $this->dropTable(self::TABLE );
    }
}
