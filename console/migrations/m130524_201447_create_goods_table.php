<?php

use yii\db\Migration;
/*
 *id int(11) Нет Нет AUTO_INCREMENT
2 category_id int(11) Нет Нет
3 pod_category_id int(11) Да NULL
4 item varchar(255) utf8_general_ci Нет Нет
5 price double Нет Нет
6 slug varchar(255) utf8_general_ci Да NULL
7 pdf varchar(255) utf8_general_ci Да NULL
8 descr text utf8_general_ci Нет Нет
9 status tinyint(4) Нет Нет
10 image varchar(100) utf8_general_ci Нет Нет
11 best tinyint(4) Да NULL
12 created_at int(11) Да NULL
13 updated_at int(11) Да NULL
14 units varchar(255) utf8_general_ci Да NULLe
 */

class m130524_201447_create_goods_table extends Migration
{
    const TABLE = '{{goods}}';

    public function up()
    {
        $this->createTable(self::TABLE , [
            'id' => $this->primaryKey()->unsigned(),
            'category_id' => $this->integer(11),
            'pod_category_id' => $this->integer(11),
            'item' => $this->string(255),
            'price' => $this->double(),
            'slug' => $this->string(255),
            'pdf' => $this->string(255),
            'descr' => $this->string(255),
            'status' => $this->smallInteger(4),
            'image' => $this->string(255),
            'best' => $this->smallInteger(4),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'units' => $this->string(255),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    public function down(){
        $this->dropTable(self::TABLE );
    }
}
