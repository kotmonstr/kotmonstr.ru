<?php

use yii\db\Migration;

/*
 * id int(11) Нет Нет AUTO_INCREMENT
2 name varchar(255) utf8_general_ci Нет Нет
3 descr text utf8_general_ci Нет Нет
4 slug varchar(255) utf8_general_ci Да NULL
5 image varchar(255) utf8_general_ci Да NULL
6 image_path varchar(255) utf8_general_ci Да NULL
 */

class m130524_201450_create_goods_category_table extends Migration
{
    const TABLE = '{{goods_category}}';

    public function up()
    {
        $this->createTable(self::TABLE , [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string(255),
            'descr' => $this->string(255),
            'slug' => $this->string(255),
            'image' => $this->string(255),
            'image_path' => $this->string(255),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    public function down(){
        $this->dropTable(self::TABLE );
    }
}
