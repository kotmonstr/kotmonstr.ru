<?php

use yii\db\Migration;

/*
id int(11) Нет Нет AUTO_INCREMENT
2 name varchar(255) utf8_general_ci Да NULL
3 description varchar(255) utf8_general_ci Да NULL
4 code varchar(255) utf8_general_ci Да NULL
5 status tinyint(4) Да NULL
6 order int(11) Да NULL
7 slug varchar(11) utf8_general_ci Да NULL

 */

class m130524_201458_create_canal_table extends Migration
{
    const TABLE = '{{canal}}';

    public function up()
    {
        $this->createTable(self::TABLE , [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string(255),
            'description' => $this->string(255),
            'code' => $this->string(255),
            'status' => $this->smallInteger(),
            'order' => $this->smallInteger(),
            'slug' => $this->string(255),

        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    public function down(){
        $this->dropTable(self::TABLE );
    }
}
