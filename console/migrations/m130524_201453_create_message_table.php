<?php

use yii\db\Migration;

/*
 id int(11) Нет Нет AUTO_INCREMENT
2 message text utf8_general_ci Да NULL
3 subject varchar(255) utf8_general_ci Да NULL
4 email varchar(255) utf8_general_ci Да NULL
5 created_at int(11) Да NULL
6 updated_at int(11) Да NULL
7 user_name varchar(255) utf8_general_ci Да NULL
8 status int(4) Да NULL
 */

class m130524_201453_create_message_table extends Migration
{
    const TABLE = '{{message}}';

    public function up()
    {
        $this->createTable(self::TABLE , [
            'id' => $this->primaryKey()->unsigned(),
            'message' => $this->string(255),
            'subject' => $this->string(255),
            'email' =>  $this->string(255),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'user_name' => $this->string(255),
            'status' => $this->smallInteger(4),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    public function down(){
        $this->dropTable(self::TABLE );
    }
}
