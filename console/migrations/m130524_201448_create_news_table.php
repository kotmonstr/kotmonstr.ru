<?php

use yii\db\Migration;
/*
 *  id int(11) Нет Нет AUTO_INCREMENT
2 title varchar(255) utf8_general_ci Нет Нет
3 image varchar(255) utf8_general_ci Нет Нет
4 content longtext utf8_general_ci Нет Нет
5 created_at timestamp on update CURRENT_TIMESTAMP Нет CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
6 updated_at timestamp Нет 0000-00-00 00:00:00
7 author varchar(100) utf8_general_ci Да NULL
8 updater_id int(11) Да NULL
9 view int(11) Нет Нет
10 slug varchar(255) utf8_general_ci Да NULL
 */

class m130524_201448_create_news_table extends Migration
{
    const TABLE = '{{news}}';

    public function up()
    {
        $this->createTable(self::TABLE , [
            'id' => $this->primaryKey()->unsigned(),
            'title' => $this->string(255),
            'image' => $this->string(255),
            'content' => $this->text(),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'author' =>  $this->integer(11),
            'updater_id' => $this->integer(11),
            'view' =>  $this->integer(11),
            'slug' => $this->string(255),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    public function down(){
        $this->dropTable(self::TABLE );
    }
}
