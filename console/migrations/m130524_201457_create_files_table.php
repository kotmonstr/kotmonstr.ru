<?php

use yii\db\Migration;

/*
id int(255) Нет Нет AUTO_INCREMENT
2 file_name varchar(255) utf8_general_ci Да NULL
3 file_path varchar(255) utf8_general_ci Да NULL
4 file_title varchar(255) utf8_general_ci Да NULL
5 file_type varchar(255) utf8_general_ci Да NULL
6 descr varchar(255) utf8_general_ci Да NULL
7 created_at timestamp on update CURRENT_TIMESTAMP Нет CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
8 updated_at timestamp Нет 0000-00-00 00:00:00
9 image varchar(255) utf8_general_ci Да NULL
10 image_path varchar(255) utf8_general_ci Да NUL

 */

class m130524_201457_create_files_table extends Migration
{
    const TABLE = '{{files}}';

    public function up()
    {
        $this->createTable(self::TABLE , [
            'id' => $this->primaryKey()->unsigned(),
            'file_name' => $this->string(255),
            'file_path' => $this->string(255),
            'file_title' => $this->string(255),
            'file_type' => $this->string(255),
            'descr' => $this->string(255),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'image' => $this->integer(11),
            'image_path' => $this->integer(11),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    public function down(){
        $this->dropTable(self::TABLE );
    }
}
