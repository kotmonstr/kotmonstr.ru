<?php

use yii\db\Migration;

/*
id int(11) Нет Нет AUTO_INCREMENT
2 youtube_id varchar(255) utf8_general_ci Нет Нет
3 title varchar(255) utf8_general_ci Нет Нет
4 descr text utf8_general_ci Да NULL
5 created_at int(11) Да NULL
6 categoria int(11) Да NULL
7 author_id int(11) Да NULL
8 slug varchar(255) utf8_general_ci Нет Нет
9 video_album_id int(11) Нет Нет
 */

class m130524_201454_create_video_table extends Migration
{
    const TABLE = '{{video}}';

    public function up()
    {
        $this->createTable(self::TABLE , [
            'id' => $this->primaryKey()->unsigned(),
            'youtube_id' => $this->string(255),
            'title' => $this->string(255),
            'descr' =>  $this->text(),
            'created_at' => $this->integer(11),
            'categoria' => $this->integer(11),
            'author_id' => $this->integer(11),
            'slug' => $this->string(255),
            'video_album_id' => $this->integer(11),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    public function down(){
        $this->dropTable(self::TABLE );
    }
}
