<?php

use yii\db\Migration;


class m130524_201446_create_article_catagory_table extends Migration
{
    const TABLE = '{{article_category}}';

    public function up()
    {
        $this->createTable(self::TABLE , [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string(255),
            'description' => $this->string(255),
            'slug' => $this->string(255),
            'status' => $this->smallInteger(4),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    public function down(){
        $this->dropTable(self::TABLE );
    }
}
