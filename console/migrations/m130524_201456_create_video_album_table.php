<?php

use yii\db\Migration;

/*
id int(11) Нет Нет AUTO_INCREMENT
2 name varchar(255) utf8_general_ci Нет Нет
3 slug varchar(255) utf8_general_ci Нет Нет
4 created_at int(11) Нет Нет
5 updated_at int(11) Нет Нет

 */

class m130524_201456_create_video_album_table extends Migration
{
    const TABLE = '{{video_album}}';

    public function up()
    {
        $this->createTable(self::TABLE , [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string(255),
            'slug' => $this->string(255),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    public function down(){
        $this->dropTable(self::TABLE );
    }
}
