<?php

use yii\db\Migration;

/*


 */

class m130524_201460_create_template_table extends Migration
{
    const TABLE = '{{template}}';

    public function up()
    {
        $this->createTable(self::TABLE , [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string(255),
            'description' => $this->text(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->insert(self::TABLE, [
            'name' => '1 column',
            'description' => '1 column',
        ]);

        $this->insert(self::TABLE, [
            'name' => '2 column',
            'description' => '2 column',
        ]);

        $this->insert(self::TABLE, [
            'name' => '3 column',
            'description' => '3 column',
        ]);

        $this->insert(self::TABLE, [
            'name' => 'youtube only',
            'description' => 'youtube only',
        ]);

        $this->insert(self::TABLE, [
            'name' => 'news',
            'description' => 'news',
        ]);
    }

    public function down(){
        $this->dropTable(self::TABLE );
    }
}
