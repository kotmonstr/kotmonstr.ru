<?php

use yii\db\Migration;

/**
 * Class m200924_133157_alter_video_table
 */
class m200924_133157_alter_video_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{video}}', 'created_at', $this->integer()->notNull());
        $this->alterColumn('{{video_album}}', 'created_at', $this->integer()->notNull());
        $this->alterColumn('{{goods}}', 'descr', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{video}}', 'created_at', $this->integer()->Null());
        $this->alterColumn('{{video_album}}', 'created_at', $this->integer()->Null());
        $this->alterColumn('{{goods}}', 'descr', $this->string());
    }

}
