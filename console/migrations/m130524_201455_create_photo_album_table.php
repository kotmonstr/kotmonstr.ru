<?php

use yii\db\Migration;

/*
id int(11) UNSIGNED Нет Нет AUTO_INCREMENT
2 name varchar(255) utf8_general_ci Да NULL
3 slug varchar(255) utf8_general_ci Да NULL
4 description text utf8_general_ci Да NULL
5 created_at int(11) Да NULL
6 updated_at int(11) Да NULL

 */

class m130524_201455_create_photo_album_table extends Migration
{
    const TABLE = '{{photo_album}}';

    public function up()
    {
        $this->createTable(self::TABLE , [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string(255),
            'slug' => $this->string(255),
            'description' =>  $this->text(),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    public function down(){
        $this->dropTable(self::TABLE );
    }
}
