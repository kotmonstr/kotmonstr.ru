<?php

use yii\db\Migration;


class m130524_201443_create_article_table extends Migration
{
    const TABLE = '{{article}}';

    public function up()
    {
        $this->createTable(self::TABLE , [
            'id' => $this->primaryKey()->unsigned(),
            'article_category' => $this->string(20),
            'title' => $this->string(100),
            'image' => $this->string(255),
            'src' => $this->string(255),
            'content' => $this->text(),
            'created_at' => $this->integer(11),
            'author' => $this->string(20),
            'updater_id' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'view' => $this->integer(11),
            'slug' => $this->string(255),
            'like' => $this->integer(11),
            'status' => $this->smallInteger(4),
            'cloud' =>$this->string(255),
            'template' => $this->integer(11),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    public function down(){
        $this->dropTable(self::TABLE );
    }
}
