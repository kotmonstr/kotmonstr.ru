<?php

use yii\db\Migration;


class m130524_201451_create_goods_pod_category_table extends Migration
{
    const TABLE = '{{goods_pod_category}}';

    public function up()
    {
        $this->createTable(self::TABLE , [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string(255),
            'description' => $this->string(255),
            'slug' => $this->string(255),
            'category_id' => $this->string(255),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    public function down(){
        $this->dropTable(self::TABLE );
    }
}
