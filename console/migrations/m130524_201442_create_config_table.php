<?php

use yii\db\Migration;

class m130524_201442_create_config_table extends Migration
{
    const TABLE = '{{%config}}';
    private $data = [
        [
            'param' => 'GOODS_PER_PAGE',
            'value' => 3,
            'default' => 12,
            'label' => 'Товаров на страницу',
            'type' => 'integer',
        ],
        [
            'param' => 'SLIDER_STATUS',
            'value' => 1,
            'default' => 1,
            'label' => 'Слайдер 1-вкл , 0-выкл',
            'type' => 'integer',
        ],
        [
            'param' => 'VIDEO_PER_PAGE',
            'value' => 6,
            'default' => 6,
            'label' => 'Видео на страницу',
            'type' => 'integer',
        ],
        [
            'param' => 'SERVICE_PER_PAGE',
            'value' => 3,
            'default' => 6,
            'label' => 'Услуги на страницу',
            'type' => 'integer',
        ],
        [
            'param' => 'GALLARY_PER_PAGE',
            'value' => 12,
            'default' => 6,
            'label' => 'Картинок на страницу',
            'type' => 'integer',
        ],
        [
            'param' => 'BLOG_PER_PAGE',
            'value' => 6,
            'default' => 6,
            'label' => 'Постов на страницу',
            'type' => 'integer',
        ],
        [
            'param' => 'ADMIN_ITEMS_PER_PAGE',
            'value' => 10,
            'default' => 20,
            'label' => 'Количество предметов на страницу в админке',
            'type' => 'integer',
        ],
        [
            'param' => 'ADMIN_EMAIL',
            'value' => 'kotmonstr@ukr.net',
            'default' => 'kotmonstr@ukr.net',
            'label' => 'Email администрации',
            'type' => 'string',
        ],
        [
            'param' => 'SHOP_STATUS',
            'value' => 0,
            'default' => 1,
            'label' => 'on off',
            'type' => 'integer',
        ],
        [
            'param' => 'NEWS_PER_PAGE',
            'value' => 12,
            'default' => 12,
            'label' => 'on off',
            'type' => 'integer',
        ],
        [
            'param' => 'SITE_NAME',
            'value' => 'kotmonstr.com',
            'default' => 'kotmonstr.com',
            'label' => 'имя сайта',
            'type' => 'string',
        ],
        [
            'param' => 'SNOW_IN_LAYOUT',
            'value' => 1,
            'default' => 0,
            'label' => 'js snow in the layout',
            'type' => 'integer',
        ],
    ];

    public function up()
    {
        if (Yii::$app->db->schema->getTableSchema(self::TABLE, true) !== null) {
            $this->dropTable(self::TABLE);
        }

        $this->createTable(self::TABLE, [
            'id' => $this->primaryKey()->unsigned(),
            'param' => $this->string(255),
            'value' => $this->text(),
            'default' => $this->text(),
            'label' => $this->string(255),
            'type' => $this->string(128),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        foreach ($this->data as $line) {
            $this->insert(self::TABLE, [
                'param' => $line['param'],
                'value' => $line['value'],
                'default' => $line['default'],
                'label' => $line['label'],
                'type' => $line['type'],
            ]);
        }
    }

    public function down()
    {
        $this->dropTable(self::TABLE);
    }
}
