<?php

use yii\db\Migration;


class m130524_201445_create_image_table extends Migration
{
    const TABLE = '{{image}}';

    public function up()
    {
        $this->createTable(self::TABLE , [
            'id' => $this->primaryKey()->unsigned(),
            'name' =>  $this->string(255),
            'status' =>  $this->smallInteger(4)->defaultValue(0),
            'text' => $this->string(255),
            'cloud' => $this->string(255),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    public function down(){
        $this->dropTable(self::TABLE );
    }
}
