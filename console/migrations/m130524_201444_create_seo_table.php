<?php

use yii\db\Migration;



class m130524_201444_create_seo_table extends Migration
{
    const TABLE = '{{seo}}';

    public function up()
    {
        $this->createTable(self::TABLE , [
            'id' => $this->primaryKey()->unsigned(),
            'url' =>  $this->string(255),
            'title' =>  $this->string(255),
            'description' => $this->string(255),
            'keywords' => $this->string(255),

        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    public function down(){
        $this->dropTable(self::TABLE );
    }
}
