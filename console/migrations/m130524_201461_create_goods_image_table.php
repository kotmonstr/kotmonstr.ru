<?php

use yii\db\Migration;


class m130524_201461_create_goods_image_table extends Migration
{
    const TABLE = '{{goods_image}}';

    public function up()
    {
        $this->createTable(self::TABLE , [
            'id' => $this->primaryKey()->unsigned(),
            'good_id' => $this->integer(11),
            'name' =>  $this->string(255),
            'status_visible' =>  $this->smallInteger(4)->defaultValue(0),
            'status_main' =>  $this->smallInteger(4)->defaultValue(0),
            'cloud' => $this->string(255),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    public function down(){
        $this->dropTable(self::TABLE );
    }

}
